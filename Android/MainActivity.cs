﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Xamarin;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Instrumentation;

namespace CouchbaseBeers.Android
{
	[Activity (Label = "CouchbaseBeers",
		MainLauncher = true,
		ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			#if TRACKING
			Insights.Initialize ("bfd88409829f40c6138ef7f88cce22d0f0e9c51d", this);
			Insights.DisableCollection = false;
			Insights.DisableDataTransmission = false;
			Insights.DisableExceptionCatching = false;
			#endif

			Forms.Init (this, bundle);

			App.Initialize ();

			LoadApplication (new App ());
		}
	}
}

