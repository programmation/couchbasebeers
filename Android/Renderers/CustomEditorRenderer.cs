﻿using System;
using System.ComponentModel;
using Android.Graphics;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Extensions;
using Renderers;
using Views;

[assembly: ExportRenderer (typeof(CustomEditor), typeof(CustomEditorRenderer))]

namespace Renderers
{
	public class CustomEditorRenderer
		: EditorRenderer
	{
		public CustomEditorRenderer ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Editor> e)
		{
			base.OnElementChanged (e);

			var view = (CustomEditor)Element;

			SetFont (view);
			SetTextColor (view);
			SetMaxWidth (view);
			SetLines (view);

			ResizeHeight ();
		}

		protected override void OnElementPropertyChanged (object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			var view = (CustomEditor)Element;

			if (e.PropertyNameMatches (() => view.Font)) {
				SetFont (view);
			}
			if (e.PropertyNameMatches (() => view.TextColor)) {
				SetTextColor (view);
			}
			if (e.PropertyNameMatches (() => view.MaxWidth)) {
				SetMaxWidth (view);
			}
			if (e.PropertyNameMatches (() => view.Lines)) {
				SetLines (view);
			}

			ResizeHeight ();
		}

		private void SetFont (CustomEditor view)
		{
			//FormattedString formattedString = base.Element.FormattedText ?? base.Element.Text;
			FormattedString formattedString = base.Element.Text;

			Control.SetBackgroundColor (Android.Graphics.Color.Transparent);

			Control.TextFormatted = formattedString.ToAttributed (view.Font, view.TextColor, Control);

			if (view.Font.FontFamily != null) {
				Control.Typeface = Typeface.CreateFromAsset (Forms.Context.Assets,
					string.Format ("{0}.ttf", GetFontFamily (view.Font)));
			}
				
			if (view.Font.UseNamedSize) {
				float size;
				switch (view.Font.NamedSize) {
				case NamedSize.Large:
					size = 18.0f;
					break;
				case NamedSize.Medium:
					size = 14.0f;
					break;
				case NamedSize.Small:
					size = 11.0f;
					break;
				case NamedSize.Micro:
					size = 8.0f;
					break;
				default:
					size = 11.0f;
					break;
				}
				Control.SetTextSize (Android.Util.ComplexUnitType.Dip, size);
//				this.Control.TextSize = size;
			} else {
				if (view.Font.FontSize > 0) {
					Control.SetTextSize (Android.Util.ComplexUnitType.Dip, (float)view.Font.FontSize);
//					this.Control.TextSize = (float)view.Font.FontSize;
				}
			}
		}

		private static string GetFontFamily (Font font)
		{
			var family = font.FontFamily;

			if (family != null && family.IndexOf ("-", StringComparison.InvariantCulture) == -1)
				family += "-Regular";

			return family;
		}

		public override void SetBackgroundColor (Android.Graphics.Color color)
		{
			base.SetBackgroundColor (color);
		}

		private void SetBackgroundColor (CustomEditor view)
		{
			int r = (int)view.BackgroundColor.R * 255;
			int g = (int)view.BackgroundColor.G * 255;
			int b = (int)view.BackgroundColor.B * 255;
			int a = (int)view.BackgroundColor.A * 255;
			Control.SetBackgroundColor (new Android.Graphics.Color (r, g, b, a));
		}

		private void SetTextColor (CustomEditor view)
		{
			int r = (int)view.TextColor.R * 255;
			int g = (int)view.TextColor.G * 255;
			int b = (int)view.TextColor.B * 255;
			int a = (int)view.TextColor.A * 255;
			Control.SetTextColor (new Android.Graphics.Color (r, g, b, a));
		}

		private float _maxWidth = -1.0f;

		private void SetMaxWidth (CustomEditor view)
		{
			_maxWidth = view.MaxWidth;
		}

		private float _lines = -1.0f;

		private void SetLines (CustomEditor view)
		{
			_lines = view.Lines;
		}

		private void ResizeHeight ()
		{
			if (Element.HeightRequest > 0) {
				return;
			}

			var maxHeight = -1.0f;

			var text = Control.Text;

			if (_lines > 0) {
				this.Control.SetMinLines ((int)Math.Ceiling ((double)_lines));
//				maxHeight = this.Control.TextSize * _lines;
			} else if (_maxWidth > 0) {
//				var rect = text.GetBoundingRect (new SizeF (_maxWidth, float.MaxValue), 
//					           MonoTouch.Foundation.NSStringDrawingOptions.UsesLineFragmentOrigin,
//					           null);
//				maxHeight = rect.Height;
			} else {
//				var boundsHeight = Bounds.Height;
//				var contentHeight = Control.IntrinsicContentSize.Height;
//				maxHeight = Math.Max (boundsHeight, contentHeight);
			}
//			if (maxHeight > 0) {
//				Element.HeightRequest = maxHeight;
//			} else {
//				Element.HeightRequest = -1.0f;
//			}
		}
	}
}

