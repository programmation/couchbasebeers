﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

using Xamarin;
using Xamarin.Forms;
using Extensions;
using System.Threading.Tasks;
using Xamarin.Forms.Platform.iOS;

namespace CouchbaseBeers.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate 
		: FormsApplicationDelegate
	{
		UIWindow window;

		public override bool FinishedLaunching (UIApplication uiApplication, NSDictionary launchOptions)
		{
			#if TRACKING
			Insights.Initialize ("bfd88409829f40c6138ef7f88cce22d0f0e9c51d");
			Insights.DisableCollection = false;
			Insights.DisableDataTransmission = false;
			Insights.DisableExceptionCatching = false;
			#endif

			Forms.Init ();

			App.Initialize ();

			LoadApplication (new App ());

			return base.FinishedLaunching (uiApplication, launchOptions);
		}
	}
}

