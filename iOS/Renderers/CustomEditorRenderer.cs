﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using Views;
using Extensions;
using Renderers;
using MonoTouch.UIKit;
using System.Drawing;
using System.ComponentModel;
using MonoTouch.ObjCRuntime;

[assembly: ExportRenderer (typeof(CustomEditor), typeof(CustomEditorRenderer))]

namespace Renderers
{
	public class CustomEditorRenderer
		: EditorRenderer
	{
		public CustomEditorRenderer ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Editor> e)
		{
			base.OnElementChanged (e);

			var view = (CustomEditor)Element;

			if (view == null) {
				return;
			}

			SetFont (view);
			SetTextColor (view);
			SetMaxWidth (view);
			SetLines (view);

			ResizeHeight ();
		}

		protected override void OnElementPropertyChanged (object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			var view = (CustomEditor)Element;

			if (e.PropertyNameMatches (() => view.Font)) {
				SetFont (view);
			}
			if (e.PropertyNameMatches (() => view.TextColor)) {
				SetTextColor (view);
			}
			if (e.PropertyNameMatches (() => view.MaxWidth)) {
				SetMaxWidth (view);
			}
			if (e.PropertyNameMatches (() => view.Lines)) {
				SetLines (view);
			}
				
			ResizeHeight ();
		}

		private void SetFont (CustomEditor view)
		{
			UIFont font;
			if (view.Font != Font.Default && (font = view.Font.ToUIFont ()) != null) {
				Control.Font = font;
			} else if (view.Font == Font.Default) {
				Control.Font = UIFont.SystemFontOfSize (17.0f);
			}
		}

		private void SetTextColor (CustomEditor view)
		{
			UIColor color;
			if (view.TextColor != Color.Default && (color = view.TextColor.ToUIColor ()) != null) {
				Control.TextColor = color;
			} else if (view.TextColor == Color.Default) {
				Control.TextColor = UIColor.DarkTextColor;
			}
		}

		private float _maxWidth = -1.0f;

		private void SetMaxWidth (CustomEditor view)
		{
			_maxWidth = view.MaxWidth;
		}

		private float _lines = -1.0f;

		private void SetLines (CustomEditor view)
		{
			_lines = view.Lines;
		}

		private void ResizeHeight ()
		{
			if (Control.RespondsToSelector (new Selector ("setTextContainerInset:"))) {
				Control.TextContainerInset = new UIEdgeInsets (0, 0, 0, 0);
			}

			if (Element.HeightRequest > 0) {
				return;
			}

			var maxHeight = -1.0f;

			var text = Control.AttributedText;

			if (_lines > 0) {
				maxHeight = Control.Font.LineHeight * _lines;
			} else if (_maxWidth > 0) {
				var rect = text.GetBoundingRect (new SizeF (_maxWidth, float.MaxValue), 
					           MonoTouch.Foundation.NSStringDrawingOptions.UsesLineFragmentOrigin,
					           null);
				maxHeight = rect.Height;
			} else {
				var boundsHeight = Bounds.Height;
				var contentHeight = Control.IntrinsicContentSize.Height;
				maxHeight = Math.Max (boundsHeight, contentHeight);
			}
			if (maxHeight > 0) {
				Control.Frame = new RectangleF (0.0f, 0.0f, (float)Element.Width, maxHeight);
				Element.HeightRequest = maxHeight;
			} else {
				Element.HeightRequest = -1.0f;
			}
		}
	}
}

