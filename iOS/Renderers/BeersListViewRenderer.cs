﻿using Pages;
using ViewModels;
using Xamarin.Forms;
using MonoTouch.UIKit;
using Renderers;
using Xamarin.Forms.Platform.iOS;
using System;
using System.Diagnostics;
using Views;
using Cells;

[assembly: ExportRenderer (typeof(BeersListView), typeof(BeersListViewRenderer))]

namespace Renderers
{
	public class BeersListViewRenderer
		: DynamicRowHeightListViewRenderer
	{
		public BeersListViewRenderer ()
		{
			HasUnevenRows = BeerCell.HasUnevenRows;
			EstimatedRowHeight = BeerCell.DefaultRowHeight;
			EstimatedSectionHeaderHeight = BeerHeaderCell.DefaultRowHeight;
		}

		protected override void OnElementChanged (ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged (e);
		}
	}
}

