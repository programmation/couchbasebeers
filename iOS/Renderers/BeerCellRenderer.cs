﻿using System;
using Renderers;
using Xamarin.Forms;
using Cells;
using Xamarin.Forms.Platform.iOS;
using MonoTouch.UIKit;

//[assembly: ExportRenderer (typeof(BeerCell), typeof(BeerCellRenderer))]

namespace Renderers
{
	public class BeerCellRenderer
		: ViewCellRenderer
	{
		public BeerCellRenderer ()
		{
		}

		public override UITableViewCell GetCell (Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var cell = base.GetCell (item, reusableCell, tv);
			return cell;
		}
	}
}

