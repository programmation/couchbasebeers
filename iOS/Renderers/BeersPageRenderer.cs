﻿using Pages;
using ViewModels;
using Xamarin.Forms;
using MonoTouch.UIKit;
using Renderers;
using Xamarin.Forms.Platform.iOS;

//[assembly: ExportRenderer (typeof(BeersPage), typeof(BeersPageRenderer))]

namespace Renderers
{
	public class BeersPageRenderer
		: PageRendererBase
	{
		public BeersPageRenderer ()
		{
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			var view = NativeView;

			var icon = new UIBarButtonItem ();
			icon.Image = UIImage.FromFile ("couchbase.png");

		}
	}
}

