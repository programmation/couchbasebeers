﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using Extensions;
using Renderers;
using System.ComponentModel;
using Pages;
using ViewModels;

//[assembly: ExportRenderer (typeof(BeerPage), typeof(BeerPageRenderer))]

namespace Renderers
{
	public class BeerPageRenderer
		: PageRendererBase
	{
		private string _name;

		public string Name { 
			get {
				return _name;
			}  
			set {
				if (_name != value) {
					_name = value;
					SetNeedsLayout ();
				}
			}
		}

		private string _style;
		private string _description;
		private double _abv;

		public BeerPageRenderer ()
		{
			KillCompilerWarnings (_style, _description, _abv);
		}

		protected override void OnElementChanged (ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged (e);
		}

		protected override void OnElementPropertyChanged (object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			var view = sender as BeerPage;

			if (e.PropertyNameMatches (() => view.BindingContext)) {
				var binding = view.BindingContext as BeerViewModel;
				if (binding != null) {
					Name = binding.Name;
					_style = binding.Style;
					_description = binding.Description;
					_abv = binding.Abv;
				}
				LayoutIfNeeded ();
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
		}
	}
}

