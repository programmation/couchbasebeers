﻿using System;
using Xamarin.Forms.Platform.iOS;
using MonoTouch.UIKit;
using System.Diagnostics;
using Xamarin.Forms;
using Renderers;
using Services;

//[assembly: ExportRenderer (typeof(ScrollView), typeof(CustomScrollViewRenderer))]

namespace Renderers
{
	public class CustomScrollViewRenderer
		: ScrollViewRenderer
	{
		public CustomScrollViewRenderer ()
		{
			Scrolled += ScrolledHandler;
			DecelerationEnded += DecelerationEndedHandler;
		}

		void ScrolledHandler (object sender, EventArgs eventArgs)
		{
			var scrollView = sender as UIScrollView;
			if (scrollView == null) {
				return;
			}
			Logger.Debug (string.Format ("Scrolled to {0}", scrollView.ContentOffset));
		}

		void DecelerationEndedHandler (object sender, EventArgs eventArgs)
		{
			var scrollView = sender as UIScrollView;
			if (scrollView == null) {
				return;
			}
			Logger.Debug (string.Format ("Scrolled to {0}", scrollView.ContentOffset));
		}
	}
}

