﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Reflection;

// From https://github.com/EgorBo/CrossChat-Xamarin.Forms/blob/master/Client/Crosschat.Client.iOS/CustomRenderers/ChatListViewRenderer.cs
using System.Drawing;

namespace Renderers
{
	public class DynamicRowHeightListViewRenderer : ListViewRenderer
	{
		public bool HasUnevenRows { get; set; }

		public float EstimatedRowHeight { get; set; }

		public float EstimatedSectionHeaderHeight { get; set; }

		protected override void OnElementChanged (ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged (e);
			var table = (UITableView)this.Control;
			table.EstimatedRowHeight = EstimatedRowHeight;
			table.EstimatedSectionHeaderHeight = EstimatedSectionHeaderHeight;
			table.Source = new ListViewDataSourceWrapper (this.GetFieldValue<UITableViewSource> (typeof(ListViewRenderer), "dataSource"));
		}
	}

	public class ListViewDataSourceWrapper : UITableViewSource
	{
		private readonly UITableViewSource underlyingTableSource;

		public ListViewDataSourceWrapper (UITableViewSource underlyingTableSource)
		{
			this.underlyingTableSource = underlyingTableSource;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			return this.GetCellInternal (tableView, indexPath);
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			return this.underlyingTableSource.RowsInSection (tableview, section);
		}

		public override float GetHeightForHeader (UITableView tableView, int section)
		{
			return this.underlyingTableSource.GetHeightForHeader (tableView, section);
		}

		public override UIView GetViewForHeader (UITableView tableView, int section)
		{
			return this.underlyingTableSource.GetViewForHeader (tableView, section);
		}

		public override int NumberOfSections (UITableView tableView)
		{
			return this.underlyingTableSource.NumberOfSections (tableView);
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			this.underlyingTableSource.RowSelected (tableView, indexPath);
		}

		public override string[] SectionIndexTitles (UITableView tableView)
		{
			return this.underlyingTableSource.SectionIndexTitles (tableView);
		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			return this.underlyingTableSource.TitleForHeader (tableView, section);
		}

		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			var tableViewCell = this.GetCellInternal (tableView, indexPath);

			tableViewCell.SetNeedsLayout ();
			tableViewCell.LayoutIfNeeded ();

			var tableViewCellType = tableViewCell.GetType ();

			var viewCell = tableViewCell.GetPropertyValue<ViewCell> (tableViewCellType, "ViewCell");

			var viewCellType = viewCell.GetType ();

			var renderer = RendererFactory.GetRenderer (viewCell.View);

			return (float)viewCell.RenderHeight;
		}

		private UITableViewCell GetCellInternal (UITableView tableView, NSIndexPath indexPath)
		{
			return this.underlyingTableSource.GetCell (tableView, indexPath);
		}
	}

	public static class PrivateExtensions
	{
		public static T GetFieldValue<T> (this object @this, Type type, string name)
		{
			var field = type.GetField (name, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField);
			return (T)field.GetValue (@this);
		}

		public static T GetPropertyValue<T> (this object @this, Type type, string name)
		{
			var property = type.GetProperty (name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty);
			return (T)property.GetValue (@this);
		}
	}
}

