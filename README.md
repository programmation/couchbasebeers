# README #

Welcome to Couchbase Beers!

### Couchbase Beers ###

* This is a quick-and-dirty demo of the Couchbase Mobile platform.
* It links iPhone and Android devices running a Xamarin.Forms app to Couchbase Server via the Couchbase Sync Gateway.

### How do I get set up? ###

* You need to download and install Couchbase Server and Sync Gateway separately. Please see the www.couchbase.com web site for more details.
* To compile and run this software you need the Xamarin development stack. I used Xamarin Studio on Mac OS X to create the project, but you may also work with Visual Studio on Windows if you wish.

### Contribution guidelines ###

* Please make any suggestions or changes on a branch and submit a pull request. Thanks!

### Who do I talk to? ###

david@programmation.com.au