﻿using System;
using Xamarin.Forms;

namespace ValueConverters
{
	public class AlcoholByVolumeValueConverter
		: IValueConverter
	{
		private string _positiveFormat;
		private string _zeroFormat;

		public AlcoholByVolumeValueConverter (string positiveFormat, string zeroFormat)
		{
			_positiveFormat = positiveFormat;
			_zeroFormat = zeroFormat;
		}

		#region IValueConverter implementation

		public object Convert (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			object result = 0.ToString (_zeroFormat);
			var actualValue = (double)value;
			if (actualValue > 0) {
				result = actualValue.ToString (_positiveFormat);
			}
			return result;
		}

		public object ConvertBack (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException ();
		}

		#endregion
	}
}

