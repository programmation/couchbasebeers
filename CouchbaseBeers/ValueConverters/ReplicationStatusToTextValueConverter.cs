﻿using System;
using Xamarin.Forms;
using System.Globalization;
using Couchbase.Lite;

namespace ValueConverters
{
	public class ReplicationStatusToTextValueConverter : IValueConverter
	{
		public ReplicationStatusToTextValueConverter ()
		{
		}

		#region IValueConverter implementation

		public object Convert (object value, Type targetType, object parameter, CultureInfo culture)
		{
			var prefix = parameter != null ? ((string)parameter) + ": " : "";
			var text = "";
			switch ((ReplicationStatus)value) {
			case ReplicationStatus.Active:
				text = "Active";
				break;
			case ReplicationStatus.Idle:
				text = "Idle";
				break;
			case ReplicationStatus.Offline:
				text = "Offline";
				break;
			case ReplicationStatus.Stopped:
				text = "Stopped";
				break;
			default:
				break;
			}

			return prefix + text;
		}

		public object ConvertBack (object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException ();
		}

		#endregion
	}

}

