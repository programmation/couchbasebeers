﻿using System;
using Xamarin.Forms;
using System.Globalization;

namespace ValueConverters
{
	public class NumberToTextValueConverter : IValueConverter
	{
		readonly string _trueValue;
		readonly string _falseValue;

		public NumberToTextValueConverter (string trueValue, string falseValue)
		{
			_falseValue = falseValue;
			_trueValue = trueValue;
		}

		#region IValueConverter implementation

		public object Convert (object value, Type targetType, object parameter, CultureInfo culture)
		{
			return (float)value > 0 ? _trueValue : _falseValue;
		}

		public object ConvertBack (object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException ();
		}

		#endregion
	}
}

