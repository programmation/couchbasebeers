﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace ValueConverters
{
	public class NegateNumberToBooleanConverter : IValueConverter
	{
		public NegateNumberToBooleanConverter ()
		{
		}

		#region IValueConverter implementation

		public object Convert (object value, Type targetType, object parameter, CultureInfo culture)
		{
			var result = (float)value;
			return result > 0 ? false : true;
		}

		public object ConvertBack (object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException ();
		}

		#endregion
	}
}

