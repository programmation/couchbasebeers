﻿using Xamarin.Forms;
using System;

/*
 * http://forums.xamarin.com/discussion/comment/57708/#Comment_57708
 */

namespace ValueConverters
{
	public class CustomValueConverter
	{
		public static IValueConverter OneWay<TFrom, TTo> (Func<TFrom, TTo> converter)
		{
			return new CustomValueConverterImpl<TFrom, TTo> (converter, null);
		}

		public static IValueConverter TwoWay<TFrom, TTo> (Func<TFrom, TTo> converter, 
		                                                  Func<TTo, TFrom> backConverter)
		{
			return new CustomValueConverterImpl<TFrom, TTo> (converter, backConverter);
		}

		private class CustomValueConverterImpl<TSource, TDestination> : IValueConverter
		{
			private Func<TSource, TDestination> _converter;
			private Func<TDestination, TSource> _backConverter;

			public CustomValueConverterImpl (Func<TSource, TDestination> converter, 
			                                 Func<TDestination, TSource> backConverter)
			{
				_converter = converter;
				_backConverter = backConverter;
			}

			public object Convert (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
			{
				return _converter ((TSource)value);
			}

			public object ConvertBack (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
			{
				if (_backConverter == null)
					throw new NotImplementedException ();

				return _backConverter ((TDestination)value);
			}
		}
	}
}
