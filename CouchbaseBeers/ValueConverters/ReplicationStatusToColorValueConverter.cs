﻿using System;
using Xamarin.Forms;
using System.Globalization;
using Couchbase.Lite;

namespace ValueConverters
{
	public class ReplicationStatusToColorValueConverter : IValueConverter
	{
		public ReplicationStatusToColorValueConverter ()
		{
		}

		#region IValueConverter implementation

		public object Convert (object value, Type targetType, object parameter, CultureInfo culture)
		{
			var color = Color.Default;

			switch ((ReplicationStatus)value) {
			case ReplicationStatus.Active:
				color = Color.Green;
				break;
			case ReplicationStatus.Idle:
				color = Color.FromHex ("444444");
				break;
			case ReplicationStatus.Offline:
				color = Color.Maroon;
				break;
			case ReplicationStatus.Stopped:
				color = Color.Red;
				break;
			default:
				break;
			}

			return color;
		}

		public object ConvertBack (object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException ();
		}

		#endregion
	}

}

