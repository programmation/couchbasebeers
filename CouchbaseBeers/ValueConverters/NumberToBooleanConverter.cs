﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace ValueConverters
{
	public class NumberToBooleanConverter : IValueConverter
	{
		public NumberToBooleanConverter ()
		{
		}

		#region IValueConverter implementation

		public object Convert (object value, Type targetType, object parameter, CultureInfo culture)
		{
			var result = (float)value;
			return result > 0 ? true : false;
		}

		public object ConvertBack (object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException ();
		}

		#endregion
	}
}

