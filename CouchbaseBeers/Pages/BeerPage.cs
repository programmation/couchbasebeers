﻿using System;
using Models;
using ViewModels;
using Xamarin.Forms;
using Views;
using Elements;
using ValueConverters;

namespace Pages
{
	public class BeerPage
		: CustomContentPage
	{
		private CustomElements _elements = new CustomElements ();

		public BeerPage ()
		{
		}

		public void Init ()
		{
			CreateDismissPageButton ();

			Content = CreateView ();
		}

		private View CreateView ()
		{
			var view = new StackLayout {
				BackgroundColor = LookAndFeel.Colors.PageBackgroundColor,
				Orientation = StackOrientation.Vertical,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0
			};

			view.Children.Add (CreateHeader ());
			view.Children.Add (new SeparatorView ());
			view.Children.Add (CreateDetails ());

			return view;
		}

		private View CreateHeader ()
		{
			var beerAbvLabel = new Label {
				BackgroundColor = LookAndFeel.Colors.BeerAbvColor,
				TextColor = LookAndFeel.Colors.CellFontColor,
				FontFamily = LookAndFeel.Fonts.BeerPageAbvFont.FontFamily,
				FontAttributes = LookAndFeel.Fonts.BeerPageAbvFont.FontAttributes,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				WidthRequest = 50,
				MinimumWidthRequest = 50,
			};

			var converter = new AlcoholByVolumeValueConverter ("#.#", "N/A");

			beerAbvLabel.SetBinding (Label.TextProperty, "Abv", converter: converter);

			var beerStyleLabel = new Label {
				FontFamily = LookAndFeel.Fonts.BeerPageStyleFont.FontFamily,
				FontAttributes = LookAndFeel.Fonts.BeerPageStyleFont.FontAttributes,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				LineBreakMode = LineBreakMode.WordWrap,
			};

			beerStyleLabel.SetBinding (Label.TextProperty, "Style");

			var beerHeader = new RelativeLayout {
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.Start,
			};

			beerHeader.Children.Add (beerAbvLabel,
				Constraint.Constant (0),
				Constraint.Constant (0),
				Constraint.RelativeToParent ((parent) => {
					return (parent.Width - _elements.PadLeft - _elements.PadRight) * 1.0f / 4.0f;
				})
			);

			beerHeader.Children.Add (beerStyleLabel,
				Constraint.RelativeToView (beerAbvLabel, 
					(parent, sibling) => {
						return sibling.X + sibling.Width + 2 * _elements.Spacing;
					}),
				Constraint.Constant (0),
				Constraint.RelativeToParent ((parent) => {
					return (parent.Width - _elements.PadLeft - _elements.PadRight) * 3.0f / 4.0f;
				})
			);

			var beerHeaderLayout = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				Spacing = _elements.Spacing,
				Padding = new Thickness (_elements.PadLeft, 
					_elements.PadTop, 
					_elements.PadRight, 
					_elements.PadBottom),
				Children = {
					beerHeader,
				},
			};

			var nameEditor = new LabelledEditor<BeerViewModel> (vm => vm.NameLabel, vm => vm.Name, 3.0f);
			nameEditor.BackgroundColor = LookAndFeel.Colors.PageHeaderBackgroundColor;

			var layout = new StackLayout {
				Orientation = StackOrientation.Vertical,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.Fill,
				Children = {
					beerHeaderLayout,
					nameEditor,
				},
			};

			return layout;
		}

		View CreateBrewery ()
		{
			var brewery = new Label {
				FontFamily = LookAndFeel.Fonts.LabelFont.FontFamily,
				FontAttributes = LookAndFeel.Fonts.LabelFont.FontAttributes,
			};

			return brewery;
		}

		View CreateDetails ()
		{
			var view = new LabelledEditor<BeerViewModel> (vm => vm.DescriptionLabel, vm => vm.Description, 7.0f);
			view.BackgroundColor = LookAndFeel.Colors.PageBackgroundColor;
			return view;
		}
	}
}

