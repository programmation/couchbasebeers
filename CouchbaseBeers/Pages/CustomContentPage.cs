﻿using System;
using Xamarin.Forms;
using Views;
using System.Linq.Expressions;
using Elements;
using ViewModels;
using System.Linq;

namespace Pages
{
	public class CustomContentPage
		: ContentPage
	{

		public CustomContentPage ()
		{
			Elements = new CustomElements ();

			NavigationPage.SetBackButtonTitle (this, "");

			this.SetBinding (TitleProperty, (BaseViewModel vm) => vm.Title);
			this.SetBinding (NavigationProperty, (BaseViewModel vm) => vm.Navigation);
		}

		protected void CreateDismissPageButton ()
		{
			var context = (BaseViewModel)BindingContext;
			if (context == null) {
				return;
			}
			var doneButtonLabel = ((BaseViewModel)BindingContext).DismissLabel;
			if (doneButtonLabel != null) {
				// Don't add the button if it's already there
				if (ToolbarItems
					.Any (x => x.Text == doneButtonLabel)) {
					return;
				}
				var doneButton = new ToolbarItem ();
				doneButton.Text = doneButtonLabel;
				doneButton.SetBinding (ToolbarItem.CommandProperty, (BaseViewModel vm) => vm.DismissCommand);
				ToolbarItems.Add (doneButton);
			}
		}

		public CustomElements Elements { get; private set; }
	}
}

