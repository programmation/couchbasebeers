﻿using System;
using ViewModels;
using Xamarin.Forms;
using Models;
using Views;
using System.Threading.Tasks;

namespace Pages
{
	public class ConfigurationPage
		: CustomContentPage
	{
		public ConfigurationPage ()
		{
		}

		public void Init ()
		{
			CreateDismissPageButton ();

			Content = CreateView ();
		}

		private View CreateView ()
		{
			var view = new StackLayout {
				BackgroundColor = LookAndFeel.Colors.PageBackgroundColor,
				Orientation = StackOrientation.Vertical,
				Spacing = Elements.Spacing
			};

			view.Children.Add (CreateServerIPAddress ());
			view.Children.Add (CreateServerPort ());
			view.Children.Add (CreateServerAdminPort ());
			view.Children.Add (new SeparatorView ());
			view.Children.Add (CreateUserName ());
			view.Children.Add (CreateUserPassword ());

			return view;
		}

		private View CreateServerIPAddress ()
		{
			var view = new LabelledEditor<ConfigurationViewModel> (vm => vm.ServerIPAddressLabel, vm => vm.ServerIPAddress);
			return view;
		}

		private View CreateServerPort ()
		{
			var view = new LabelledEditor<ConfigurationViewModel> (vm => vm.ServerPortLabel, vm => vm.ServerPort);
			return view;
		}

		private View CreateServerAdminPort ()
		{
			var view = new LabelledEditor<ConfigurationViewModel> (vm => vm.ServerAdminPortLabel, vm => vm.ServerAdminPort);
			return view;
		}

		private View CreateUserName ()
		{
			var view = new LabelledEditor<ConfigurationViewModel> (vm => vm.UserNameLabel, vm => vm.UserName);
			return view;
		}

		private View CreateUserPassword ()
		{
			var view = new LabelledEditor<ConfigurationViewModel> (vm => vm.UserPasswordLabel, vm => vm.UserPassword);
			return view;
		}
	}
}

