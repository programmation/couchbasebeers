﻿using System;
using Xamarin.Forms;

namespace Pages
{
	public class DataTemplateSelector
	{
		public Func<object, DataTemplate> Selector { get; set; }

		public DataTemplate Template {
			get { return new DataTemplate (GetHookedCell); }
		}

		Cell GetHookedCell ()
		{
			var content = new ViewCell ();
			content.BindingContextChanged += OnBindingContextChanged;
			return content;
		}

		internal void OnBindingContextChanged (object sender, EventArgs e)
		{
			var cell = (ViewCell)sender;
			var template = Selector (cell.BindingContext);
			cell.View = ((ViewCell)template.CreateContent ()).View;
		}
	}
}