﻿using System;
using PropertyChanged;
using Xamarin.Forms;
using ViewModels;
using ValueConverters;
using Views;
using Cells;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using CouchbaseBeers;
using Extensions;

namespace Pages
{
	public class BeersPage 
		: CustomContentPage
	{
		ListView _beersList;
		ProgressBar _progressBar;
		StackLayout _pullStatusBar;
		StackLayout _pushStatusBar;
		CustomSearchBar _searchBar;

		public BeersPage ()
		{
		}

		public void Init ()
		{
			var relativeLayout = new RelativeLayout {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			// Put progress bar at top immediately under title bar
			_progressBar = CreateProgressBar ();
			relativeLayout.Children.Add (_progressBar,
				Constraint.RelativeToParent ((parent) => {
					return parent.X;
				}),
				Constraint.RelativeToParent ((parent) => {
					return parent.Y;
				}),
				Constraint.RelativeToParent ((parent) => {
					return parent.Width;
				}),
				Constraint.Constant (1)
			);

			// Put pull status bar below progress bar
			_pullStatusBar = CreatePullStatusBar ();
			relativeLayout.Children.Add (_pullStatusBar,
				Constraint.RelativeToParent ((parent) => {
					return parent.X;
				}),
				Constraint.RelativeToView (_progressBar, (parent, sibling) => {
					return sibling.Y + sibling.Height;
				}),
				Constraint.RelativeToParent ((parent) => {
					return parent.Width / 2;
				})
			);

			// Put push status bar next to pull status bar
			_pushStatusBar = CreatePushStatusBar ();
			relativeLayout.Children.Add (_pushStatusBar,
				Constraint.RelativeToParent ((parent) => {
					return parent.X + parent.Width / 2;
				}),
				Constraint.RelativeToView (_pullStatusBar, (parent, sibling) => {
					return sibling.Y;
				}),
				Constraint.RelativeToParent ((parent) => {
					return parent.Width / 2;
				})
			);

			// Put search bar immediately under push status bar
			_searchBar = CreateSearchBar ();
			relativeLayout.Children.Add (_searchBar,
				Constraint.RelativeToParent ((parent) => {
					return parent.X;
				}),
				Constraint.RelativeToView (_pushStatusBar, (parent, sibling) => {
					return sibling.Y + sibling.Height;
				}),
				Constraint.RelativeToParent ((parent) => {
					return parent.Width;
				})
			);
				
			// Fill the rest of the screen with the beers list
			_beersList = CreateBeersList ();
			relativeLayout.Children.Add (_beersList,
				Constraint.RelativeToParent ((parent) => {
					return parent.X;
				}),
				Constraint.RelativeToView (_searchBar, (parent, sibling) => {
					return sibling.Y + sibling.Height;
				}),
				Constraint.RelativeToParent ((parent) => {
					return parent.Width;
				}),
				Constraint.RelativeToView (_searchBar, (parent, sibling) => {
					return parent.Height - sibling.Y - sibling.Height;
				})
			);
				
			Content = relativeLayout;
		}

		private CustomSearchBar CreateSearchBar ()
		{
			CustomSearchBar searchBar = new CustomSearchBar {
				Placeholder = "Search",
				BarTint = LookAndFeel.Colors.SearchBarTintColor,
			};
			searchBar.SetBinding (SearchBar.TextProperty, (BeersViewModel vm) => vm.SearchText);

			return searchBar;
		}

		private ProgressBar CreateProgressBar ()
		{
			ProgressBar progressBar = new ProgressBar ();
			progressBar.SetBinding (ProgressBar.ProgressProperty, (BeersViewModel vm) => vm.ReplicationProgress);
			progressBar.SetBinding (IsVisibleProperty, (BeersViewModel vm) => vm.IsReplicating);
			return progressBar;
		}

		private StackLayout CreatePullStatusBar ()
		{
			var label = new CustomLabel<BeersViewModel> (vm => vm.PullReplicationLabel);
			var value = new CustomLabel<BeersViewModel> (vm => vm.PullReplicationStatusText);
			label.TextColor = value.TextColor = LookAndFeel.Colors.ReplicationStatusTextColor;
			label.BackgroundColor = value.BackgroundColor = LookAndFeel.Colors.ReplicationStatusBackgroundColor;
			value.LineBreakMode = LineBreakMode.HeadTruncation;

			var statusBar = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.CenterAndExpand, 
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = {
					label,
					value
				}
			};
			var replicationStatusColorConverter = new ReplicationStatusToColorValueConverter ();
			statusBar.SetBinding (BackgroundColorProperty, (BeersViewModel vm) => vm.PullReplicationStatus, converter: replicationStatusColorConverter);
			return statusBar;
		}

		private StackLayout CreatePushStatusBar ()
		{
			var label = new CustomLabel<BeersViewModel> (vm => vm.PushReplicationLabel);
			var value = new CustomLabel<BeersViewModel> (vm => vm.PushReplicationStatusText);
			label.TextColor = value.TextColor = LookAndFeel.Colors.ReplicationStatusTextColor;
			label.BackgroundColor = value.BackgroundColor = LookAndFeel.Colors.ReplicationStatusBackgroundColor;
			value.LineBreakMode = LineBreakMode.HeadTruncation;

			var statusBar = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.CenterAndExpand, 
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = {
					label,
					value
				}
			};
			var replicationStatusColorConverter = new ReplicationStatusToColorValueConverter ();
			statusBar.SetBinding (BackgroundColorProperty, (BeersViewModel vm) => vm.PushReplicationStatus, converter: replicationStatusColorConverter);
			return statusBar;
		}

		private ListView CreateBeersList ()
		{
			var listView = new BeersListView ();
			listView.HasUnevenRows = BeerCell.HasUnevenRows;
			listView.RowHeight = BeerCell.DefaultRowHeight;

			return listView;
		}
	}

	public class BeersListView
		: GroupedListView<BeersViewModel, BeerCellViewModel, BeerCell, BeerHeaderCell>
	{

	}
}

