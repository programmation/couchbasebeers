﻿using System;
using Xamarin.Forms;
using System.ComponentModel;
using ViewModels;
using Cells;

//using Controls;
using ValueConverters;

namespace Pages
{
	public class MenuPage 
		: ContentPage, INotifyPropertyChanged
	{
		public MenuPage ()
		{
			Title = "Menu";
			Icon = "menu.png";
			BackgroundColor = LookAndFeel.Colors.MenuCellColor;

			Content = new StackLayout {
				VerticalOptions = LayoutOptions.StartAndExpand,
				Children = {
					CreateTopRow (),
					CreateListView ()
				}
			};
		}

		static View CreateTopRow ()
		{
			return new StackLayout {
				HeightRequest = 50,
			};
		}

		static View CreateListView ()
		{
			var listView = new ListView () {
				RowHeight = 46,
				BackgroundColor = LookAndFeel.Colors.MenuCellColor,
				ItemTemplate = new DataTemplate (typeof(MenuCell)),
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			listView.ItemTemplate.SetBinding (MenuCell.TextProperty, "Text");
			listView.ItemTemplate.SetBinding (MenuCell.IsSelectedProperty, "IsSelected");
			listView.SetBinding (ListView.ItemsSourceProperty, "Items");
			listView.SetBinding (ListView.SelectedItemProperty, "SelectedItem");
			return listView;
		}
	}
}

