﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using Xamarin.Forms;
using ViewModels;
using Pages;
using System.Diagnostics;
using Extensions;
using TinyIoC;
using Services;

namespace Pages
{
	public class RootPage 
		: MasterDetailPage
	{
		readonly Dictionary<BaseViewModel, NavigationPage> _navigationViewModels;

		public RootPage ()
		{
			_navigationViewModels = new Dictionary<BaseViewModel, NavigationPage> ();
		}

		public void Init ()
		{
			Title = "Couchbase";

			Master = ViewModel.MasterViewModel.ResolvePage ();
			if (Device.Idiom == TargetIdiom.Phone) {
				MasterBehavior = MasterBehavior.Popover;
			} else {
				MasterBehavior = MasterBehavior.SplitOnLandscape;
			}

			NavigateToViewModel (ViewModel.DetailViewModel);

			ViewModel.PropertyChanged += ViewModelPropertyChanged;

			// is the menu able to be swiped out?
			IsGestureEnabled = false;

			SetBinding (IsPresentedProperty, new Binding ("IsPresented", BindingMode.TwoWay));

			IsPresentedChanged += (object sender, EventArgs e) => {
				if (IsPresented) {
					UnfocusAll (Detail);
				}
			};
		}

		void UnfocusAll (Page page)
		{
			NavigationPage navigationPage;
			ContentPage contentPage;

			if ((navigationPage = page as NavigationPage) != null) {
				UnfocusAll (navigationPage.CurrentPage);
			} else if ((contentPage = page as ContentPage) != null) {
				UnfocusAll (contentPage.Content);
			}
		}

		void UnfocusAll (View view)
		{
			Layout<View> layoutOfView;
			ScrollView scrollView;
			ContentView contentView;

			if (view == null)
				return;

			if ((layoutOfView = view as Layout<View>) != null) {
				foreach (var child in layoutOfView.Children) {
					UnfocusAll (child);
				}
			} else if ((scrollView = view as ScrollView) != null) {
				UnfocusAll (scrollView.Content);
			} else if ((contentView = view as ContentView) != null) {
				UnfocusAll (contentView.Content);
			} else {
				view.Unfocus ();
			}
		}

		private RootViewModel ViewModel {
			get {
				return (RootViewModel)BindingContext;
			}
			set {
				BindingContext = value;
			}
		}

		void ViewModelPropertyChanged (object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "DetailViewModel") {
				NavigateToViewModel (ViewModel.DetailViewModel);
			}
		}

		void NavigateToViewModel (BaseViewModel viewModel)
		{
			NavigationPage navPage;

			try {
				if (!_navigationViewModels.TryGetValue (viewModel, out navPage)) {
					var page = viewModel.ResolvePage ();
					navPage = new NavigationPage (page) {
						BarTextColor = LookAndFeel.Colors.NavigationBarTextColor,
						BarBackgroundColor = LookAndFeel.Colors.NavigationBarBackgroundColor,
					};
					_navigationViewModels.Add (viewModel, navPage);
				}

				Detail = navPage;
				if (MasterBehavior != MasterBehavior.SplitOnLandscape &&
				    MasterBehavior != MasterBehavior.SplitOnPortrait) {
					IsPresented = false;
				}
			} catch (Exception exception) {
				Logger.Error (exception);
			}
		}
	}
}

