﻿using System;
using ViewModels;
using Xamarin.Forms;
using Models;
using Views;
using System.Threading.Tasks;

namespace Pages
{
	public class SyncPage
		: CustomContentPage
	{
		public SyncPage ()
		{
		}

		public void Init ()
		{
			CreateDismissPageButton ();

			Content = CreateView ();
		}

		private View CreateView ()
		{
			var view = new StackLayout {
				Orientation = StackOrientation.Vertical,
				Spacing = Elements.Spacing
			};

			view.Children.Add (CreateAutoStartSwitch ());
			view.Children.Add (CreateStartSyncButton ());
			view.Children.Add (CreateStopSyncButton ());
			view.Children.Add (CreateSyncStatusLabel ());

			return view;
		}

		private View CreateAutoStartSwitch ()
		{
			var view = new LabelledSwitch<SyncViewModel> (vm => vm.AutoStartSyncLabel, vm => vm.AutoStartSync);
			return view;
		}

		private View CreateStartSyncButton ()
		{
			var view = new CustomButton<SyncViewModel> (vm => vm.StartSyncLabel, vm => vm.StartSync);
			return view;
		}

		private View CreateStopSyncButton ()
		{
			var view = new CustomButton<SyncViewModel> (vm => vm.StopSyncLabel, vm => vm.StopSync);
			return view;
		}

		private View CreateSyncStatusLabel ()
		{
			var view = new CustomLabel<SyncViewModel> (vm => vm.SyncStatus);
			view.HorizontalOptions = LayoutOptions.Center;
			view.XAlign = TextAlignment.Center;
			view.SetBinding (Label.TextColorProperty, (SyncViewModel vm) => vm.SyncStatusColor);
			return view;
		}
	}
}

