﻿using System;
using CouchbaseBeers;
using Services;
using Models;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ViewModels
{
	public class SyncViewModel
		: BaseViewModel
	{
		private AppConfig _appConfig;

		private IRepository _repository;
		private IDataService _dataService;

		public SyncViewModel (IRepository repository)
		{
			_repository = repository;
			_dataService = repository.DataService;

			Title = "Sync";

			AutoStartSyncLabel = "Auto Start";

			StartSyncLabel = "Start";
			StopSyncLabel = "Stop";

			DismissLabel = "Update";
			DismissCommand = SaveAppConfig;

			_appConfig = new AppConfig ();

			UpdateSyncStatus ();
		}

		public override Task Initialize (params object[] args)
		{
			var task = base.Initialize (args);

			_appConfig = _dataService.GetAppConfig ();

			AutoStartSync = _appConfig.AutoStartSync;

			UpdateSyncStatus ();

			return task;
		}

		private string _autoStartSyncLabel;

		public string AutoStartSyncLabel {
			get {
				return _autoStartSyncLabel;
			}
			set {
				SetObservableProperty (ref _autoStartSyncLabel, value);
			}
		}

		private bool _autoStartSync;

		public bool AutoStartSync {
			get {
				return _autoStartSync;
			}
			set {
				SetObservableProperty (ref _autoStartSync, value);
				_appConfig.AutoStartSync = _autoStartSync;
			}
		}

		private string _syncStatus;

		public string SyncStatus {
			get {
				return _syncStatus;
			}
			set {
				SetObservableProperty (ref _syncStatus, value);
			}
		}

		private Color _syncStatusColor;

		public Color SyncStatusColor {
			get {
				return _syncStatusColor;
			}
			set { 
				SetObservableProperty (ref _syncStatusColor, value);
			}
		}

		private void UpdateSyncStatus ()
		{
			SyncStatus = "Sync is " + (_dataService.IsSyncing ? "ON" : "OFF");
			if (_dataService.IsSyncing) {
				SyncStatusColor = Color.FromHex ("11BB1C");
			} else {
				SyncStatusColor = Color.FromHex ("EF0915");
			}
		}

		public ICommand SaveAppConfig {
			get {
				return new Command ((sender) => {
					_dataService.SaveAppConfig (_appConfig);
				});
			}
		}

		private string _startSyncLabel;

		public string StartSyncLabel {
			get {
				return _startSyncLabel;
			}
			set {
				SetObservableProperty (ref _startSyncLabel, value);
			}
		}

		public ICommand StartSync {
			get {
				return new Command ((sender) => {
					if (!string.IsNullOrEmpty (_appConfig.ServerIPAddress)) {
						_dataService.SaveAppConfig (_appConfig);
						Task.Run (async () => {
							await _dataService.InitializeServerConnectionAsync (false).ConfigureAwait (false);
						}).ContinueWith ((x) => {
							_dataService.StartSync ();
							UpdateSyncStatus ();
						});
					}
				});
			}
		}

		private string _stopSyncLabel;

		public string StopSyncLabel {
			get {
				return _stopSyncLabel;
			}
			set {
				SetObservableProperty (ref _stopSyncLabel, value);
			}
		}

		public ICommand StopSync {
			get {
				return new Command ((sender) => {
					_dataService.StopSync ();
					UpdateSyncStatus ();
				});
			}
		}

	}
}

