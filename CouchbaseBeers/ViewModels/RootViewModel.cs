﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using System.Threading.Tasks;
using CouchbaseBeers;
using Extensions;
using Services;

namespace ViewModels
{
	public class RootViewModel 
		: BaseViewModel
	{
		private bool _isInitialized;

		private IRepository _repository;

		public RootViewModel (IRepository repository)
		{
			_repository = repository;

			MasterViewModel = new MenuViewModel (_repository);
			DetailViewModel = MasterViewModel.SelectedItem.ViewModel;

//			MessagingCenter.Subscribe<MenuViewModel> (this, "NavigateToViewModel", 
//				(sender) => {
//					DetailViewModel = sender.SelectedItem.ViewModel;
//				});

			MessagingCenter.Subscribe<MenuItemViewModel> (this, "Tapped", sender => {
				DetailViewModel = sender.ViewModel;
			});

			if (!_isInitialized) {
				Task.Run (async () => {
					await App.InitializeAsync ()
						.ConfigureAwait (false);
				}).ContinueWith ((x) => {
					_isInitialized = true;
				});
			}
		}

		MenuViewModel _masterViewModel;

		public MenuViewModel MasterViewModel {
			get { 
				return _masterViewModel;
			}
			set {
				SetObservableProperty (ref _masterViewModel, value);
			}
		}

		BaseViewModel _detailViewModel;

		public BaseViewModel DetailViewModel {
			get {
				return _detailViewModel;
			}
			set {
				SetObservableProperty (ref _detailViewModel, value);
			}
		}

		bool _isPresented;

		public bool IsPresented {
			get {
				return _isPresented;
			}
			set {
				SetObservableProperty (ref _isPresented, value);
			}
		}
	}
}

