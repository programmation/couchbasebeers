﻿using System;
using Services;
using System.Threading.Tasks;
using Models;
using CouchbaseBeers;
using System.Windows.Input;
using Xamarin.Forms;

namespace ViewModels
{
	public class BeerViewModel
		: BaseViewModel
	{
		private readonly IRepository _repository;

		private string _beerId;

		private Beer _beer;

		public BeerViewModel (IRepository repository)
		{
			_repository = repository;

			_beer = new Beer ();

			Title = "Beer";

			NameLabel = "Name";
			DescriptionLabel = "Desc";

			DismissLabel = "Done";
			DismissCommand = SaveBeer;
		}

		public override Task Initialize (params object[] args)
		{
			var task = base.Initialize (args);

			_beerId = (string)args [0];
			_beer = _repository.GetBeer (_beerId);
					
			Name = _beer.Name;
			Abv = _beer.Abv;
			Style = _beer.Style ?? "";

			Description = _beer.Description ?? "";

			return task;
		}

		private string _id;

		public string Id {
			get {
				return _id;
			}
			set {
				SetObservableProperty (ref _id, value);
				_beer.Id = _id;
			}
		}

		private string _nameLabel;

		public string NameLabel {
			get {
				return _nameLabel;
			}
			set {
				SetObservableProperty (ref _nameLabel, value);
			}
		}

		private string _name;

		public string Name {
			get {
				return _name;
			}
			set {
				SetObservableProperty (ref _name, value);
				_beer.Name = _name;
			}
		}

		private string _breweryId;

		public string BreweryId {
			get {
				return _breweryId;
			}
			set {
				SetObservableProperty (ref _breweryId, value);
				_beer.BreweryId = _breweryId;
			}
		}

		private string _brewery;

		public string Brewery {
			get {
				return _brewery;
			}
			set {
				SetObservableProperty (ref _brewery, value);
			}
		}

		private double _abv;

		public double Abv {
			get {
				return _abv;
			}
			set {
				SetObservableProperty (ref _abv, value);
				_beer.Abv = _abv;
			}
		}

		private string _style;

		public string Style {
			get {
				return _style;
			}
			set {
				SetObservableProperty (ref _style, value);
				_beer.Style = _style;
			}
		}

		private string _descriptionLabel;

		public string DescriptionLabel {
			get {
				return _descriptionLabel;
			}
			set {
				SetObservableProperty (ref _descriptionLabel, value);
			}
		}

		private string _description;

		public string Description {
			get {
				return _description;
			}
			set {
				SetObservableProperty (ref _description, value);
				_beer.Description = _description;
			}
		}

		public ICommand SaveBeer {
			get {
				return new Command ((sender) => {
					if (!string.IsNullOrEmpty (_beer.Name)) {
						_repository.SaveBeer (_beer);
						Navigation.PopAsync ();
					}
				});
			}
		}
	}
}

