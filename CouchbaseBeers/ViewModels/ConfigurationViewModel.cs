﻿using System;
using CouchbaseBeers;
using Services;
using Models;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ViewModels
{
	public class ConfigurationViewModel
		: BaseViewModel
	{
		private AppConfig _appConfig;

		private IRepository _repository;
		private IDataService _dataService;

		public ConfigurationViewModel (IRepository repository)
		{
			_repository = repository;
			_dataService = repository.DataService;

			Title = "Configuration";

			ServerIPAddressLabel = "Server IP";
			ServerPortLabel = "Server Port";
			ServerAdminPortLabel = "Admin Port";

			UserNameLabel = "User Id";
			UserPasswordLabel = "Password";

			DismissLabel = "Update";
			DismissCommand = SaveAppConfig;

			_appConfig = new AppConfig ();
		}

		public override Task Initialize (params object[] args)
		{
			var task = base.Initialize (args);
		
			_appConfig = _dataService.GetAppConfig ();

			ServerIPAddress = _appConfig.ServerIPAddress;
			ServerPort = _appConfig.ServerPort;
			ServerAdminPort = _appConfig.ServerAdminPort;

			UserName = _appConfig.UserId;
			UserPassword = _appConfig.UserPassword;

			return task;
		}

		private string _serverIPAddressLabel;

		public string ServerIPAddressLabel {
			get {
				return _serverIPAddressLabel;
			}
			set {
				SetObservableProperty (ref _serverIPAddressLabel, value);
			}
		}

		private string _serverIPAddress;

		public string ServerIPAddress {
			get {
				return _serverIPAddress;
			}
			set {
				SetObservableProperty (ref _serverIPAddress, value);
				_appConfig.ServerIPAddress = _serverIPAddress;
			}
		}

		private string _serverPortLabel;

		public string ServerPortLabel {
			get {
				return _serverPortLabel;
			}
			set {
				SetObservableProperty (ref _serverPortLabel, value);
			}
		}

		private int _serverPort;

		public int ServerPort {
			get {
				return _serverPort;
			}
			set {
				SetObservableProperty (ref _serverPort, value);
				_appConfig.ServerPort = _serverPort;
			}
		}

		private string _serverAdminPortLabel;

		public string ServerAdminPortLabel {
			get {
				return _serverAdminPortLabel;
			}
			set {
				SetObservableProperty (ref _serverAdminPortLabel, value);
			}
		}

		private int _serverAdminPort;

		public int ServerAdminPort {
			get {
				return _serverAdminPort;
			}
			set {
				SetObservableProperty (ref _serverAdminPort, value);
				_appConfig.ServerAdminPort = _serverAdminPort;
			}
		}

		private string _userNameLabel;

		public string UserNameLabel {
			get {
				return _userNameLabel;
			}
			set {
				SetObservableProperty (ref _userNameLabel, value);
			}
		}

		private string _userName;

		public string UserName {
			get {
				return _userName;
			}
			set {
				SetObservableProperty (ref _userName, value);
				_appConfig.UserId = _userName;
			}
		}

		private string _userPasswordLabel;

		public string UserPasswordLabel {
			get {
				return _userPasswordLabel;
			}
			set {
				SetObservableProperty (ref _userPasswordLabel, value);
			}
		}

		private string _userPassword;

		public string UserPassword {
			get {
				return _userPassword;
			}
			set {
				SetObservableProperty (ref _userPassword, value);
				_appConfig.UserPassword = _userPassword;
			}
		}

		public ICommand SaveAppConfig {
			get {
				return new Command ((sender) => {
					if (!string.IsNullOrEmpty (_appConfig.ServerIPAddress)) {
						_dataService.SaveAppConfig (_appConfig);
					}
				});
			}
		}
	}
}

