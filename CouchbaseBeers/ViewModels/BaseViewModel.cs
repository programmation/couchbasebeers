﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using System.Windows.Input;

namespace ViewModels
{
	public interface IBaseViewModel
		: INotifyPropertyChanged
	{
		Task Initialize (params object[] args);
	}

	public class BaseViewModel
		: IBaseViewModel
	{
		public INavigation Navigation { get; set; }

		#region IBaseViewModel implementation

		public virtual Task Initialize (params object[] args)
		{
			return Task.FromResult (0);
		}

		#endregion

		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		private string _title;

		public string Title {
			get {
				return _title;
			}
			set {
				SetObservableProperty (ref _title, value);
			}
		}

		private string _dismissLabel;

		public string DismissLabel {
			get {
				return _dismissLabel;
			}
			set {
				SetObservableProperty (ref _dismissLabel, value);
			}
		}

		private ICommand _dismissCommand;

		public ICommand DismissCommand {
			get {
				return _dismissCommand;
			}
			set {
				SetObservableProperty (ref _dismissCommand, value);
			}
		}

		protected virtual void OnPropertyChanged (string propertyName)
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
		}

		protected void SetObservableProperty<T> (
			ref T field, 
			T value,
			[CallerMemberName] string propertyName = "")
		{
			if (EqualityComparer<T>.Default.Equals (field, value))
				return;
			field = value;
			OnPropertyChanged (propertyName);
		}

		public BaseViewModel ()
		{
		}
	}
}

