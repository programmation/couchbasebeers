﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Couchbase.Lite;
using Xamarin.Forms;
using Models;
using Pages;
using Services;
using ValueConverters;
using ObservableGrouping;
using Cells;
using Extensions;
using TinyIoC;
using System.Collections;

namespace ViewModels
{
	public class BeersViewModel
		: BaseViewModel, IGroupedListViewModel<BeerCellViewModel>
	{
		private IRepository _repository;
		private IDataService _dataService;
		private ReplicationStatusToTextValueConverter _replicationStatusTextConverter = new ReplicationStatusToTextValueConverter ();

		private ReplicationStatus _lastPullReplicationStatus;
		private ReplicationStatus _lastPushReplicationStatus;

		private TimeSpan _replicationStatusUpdateInterval = TimeSpan.FromMilliseconds (500);
		private DateTime _lastPullReplicationStatusUpdate;
		private DateTime _lastPushReplicationStatusUpdate;

		private bool _isInitialized = false;

		public BeersViewModel (IRepository repository)
		{
			_repository = repository;
			_dataService = repository.DataService;

			_lastPullReplicationStatusUpdate = DateTime.MinValue;
			_lastPushReplicationStatusUpdate = DateTime.MinValue;

			IsReplicated = _dataService.IsSyncing;
			IsReplicating = false;

			PullReplicationLabel = "Pull:";
			PushReplicationLabel = "Push:";

			MessagingCenter.Subscribe<IDataService, ReplicationMessage> (this, 
				ReplicationMessage.ReplicationMessageTag, 
				(s, e) => {
					var message = (ReplicationMessage)e;
					ReplicationProgress = message.Progress;

					var now = DateTime.Now;

					if (_lastPullReplicationStatus != message.PullStatus || now - _lastPullReplicationStatusUpdate > _replicationStatusUpdateInterval) {
						_lastPullReplicationStatus = message.PullStatus;
						_lastPullReplicationStatusUpdate = now;
						PullReplicationStatus = message.PullStatus;
						var pullStatusText = new StringBuilder ();
						pullStatusText.Append (_replicationStatusTextConverter.Convert (message.PullStatus, typeof(string), null, CultureInfo.InvariantCulture));
						if (message.PullStatus == ReplicationStatus.Active) {
							pullStatusText.Append (string.Format (" ({0} of {1})", message.PullCompleted, message.PullTotal));
						}
						PullReplicationStatusText = pullStatusText.ToString ();
					}

					if (_lastPushReplicationStatus != message.PushStatus || now - _lastPushReplicationStatusUpdate > _replicationStatusUpdateInterval) {
						_lastPushReplicationStatus = message.PushStatus;
						_lastPushReplicationStatusUpdate = now;
						PushReplicationStatus = message.PushStatus;
						var pushStatusText = new StringBuilder ();
						pushStatusText.Append (_replicationStatusTextConverter.Convert (message.PushStatus, typeof(string), null, CultureInfo.InvariantCulture));
						if (message.PushStatus == ReplicationStatus.Active) {
							pushStatusText.Append (string.Format (" ({0} of {1})", message.PushCompleted, message.PushTotal));
						}
						PushReplicationStatusText = pushStatusText.ToString ();
					}
				}
			);

			MessagingCenter.Subscribe<IDataService> (this, "Sync", (x) => {
				IsReplicated = x.IsSyncing;
			});

			Beers = new ObservableCollection<Beer> ();
		}

		public override Task Initialize (params object[] args)
		{
			if (!_isInitialized) {
				Beers = _repository.GetAllBeers ();
				SetBeersGrouped ();

				Beers.CollectionChanged += (sender, e) => {
					SetBeersGrouped ();
				};

				_isInitialized = true;
			}

			return base.Initialize ();
		}

		private void SetBeersGrouped ()
		{
			BeersGrouped = CreateBeersGrouped ();
		}

		private void UpdateTitle ()
		{
			int count = 0;
			if (ItemsSource != null) {
				var beerGroups = ItemsSource.ToList ();
				foreach (var beerGroup in beerGroups) {
					count += beerGroup.Count;
				}
			}
			if (ReplicationProgress > 0) {
				Title = string.Format ("Beers: Replicating... ({0})", count);
				return;
			}
			if (!IsReplicated) {
				Title = string.Format ("Beers: Local ({0})", count);
				return;
			}
			Title = string.Format ("Beers ({0})", count);
		}

		bool _isEmpty;

		public bool IsEmpty {
			get {
				return _isEmpty;
			}
			set {
				SetObservableProperty (ref _isEmpty, value);
			}
		}

		bool _isReplicated;

		public bool IsReplicated {
			get {
				return _isReplicated;
			}
			set {
				UpdateTitle ();
				SetObservableProperty (ref _isReplicated, value);
			}
		}

		bool _isReplicating;

		public bool IsReplicating {
			get {
				return _isReplicating;
			}
			set {
				SetObservableProperty (ref _isReplicating, value);
			}
		}

		private string _pullReplicationLabel;

		public string PullReplicationLabel {
			get {
				return _pullReplicationLabel;
			}
			set {
				SetObservableProperty (ref _pullReplicationLabel, value);
			}
		}

		private string _pushReplicationLabel;

		public string PushReplicationLabel {
			get {
				return _pushReplicationLabel;
			}
			set {
				SetObservableProperty (ref _pushReplicationLabel, value);
			}
		}

		private ReplicationStatus _pullReplicationStatus;

		public ReplicationStatus PullReplicationStatus {
			get { 
				return _pullReplicationStatus;
			}
			set {
				SetObservableProperty (ref _pullReplicationStatus, value);
			}
		}

		private ReplicationStatus _pushReplicationStatus;

		public ReplicationStatus PushReplicationStatus {
			get { 
				return _pushReplicationStatus;
			}
			set {
				SetObservableProperty (ref _pushReplicationStatus, value);
			}
		}

		private string _pullReplicationStatusText;

		public string PullReplicationStatusText {
			get {
				return _pullReplicationStatusText;
			}
			set {
				SetObservableProperty (ref _pullReplicationStatusText, value);
			}
		}

		private string _pushReplicationStatusText;

		public string PushReplicationStatusText {
			get {
				return _pushReplicationStatusText;
			}
			set {
				SetObservableProperty (ref _pushReplicationStatusText, value);
			}
		}

		private float _replicationProgress;

		public float ReplicationProgress {
			get {
				return _replicationProgress;
			}
			set {
				UpdateTitle ();
				SetObservableProperty (ref _replicationProgress, value);
			}
		}

		private ObservableCollection<Beer> _beers;

		internal ObservableCollection<Beer> Beers {
			get {
				return _beers;
			}
			set {
				_beers = value;
			}
		}

		public GroupedObservableCollection<string, BeerCellViewModel> CreateBeersGrouped ()
		{
			var cells = from beer in Beers
			            select new BeerCellViewModel (beer);

			var sorted = from cell in cells
			             orderby SafeToLower (cell.Name), SafeToLower (cell.Style)
			             group cell by cell.NameSort into BeerGroup
			             select new ObservableGrouping<string, BeerCellViewModel> (BeerGroup.Key, BeerGroup);

			var beers = new GroupedObservableCollection<string, BeerCellViewModel> (sorted);

//			Logger.Debug ("Updating grouped list of {0} beers", beers.Count);

			return beers;
		}

		private string SafeToLower (string value)
		{
			return value == null ? null : value.ToLower ();
		}

		private GroupedObservableCollection<string, BeerCellViewModel> _beersGrouped;

		public GroupedObservableCollection<string, BeerCellViewModel> BeersGrouped {
			get {
				return _beersGrouped;
			}
			set {
				SetObservableProperty (ref _beersGrouped, value);

				// check to see if you have any Beers
				IsEmpty = _beersGrouped.Count == 0;
				UpdateTitle ();
				SetBeersFiltered ();
			}
		}

		private GroupedObservableCollection<string, BeerCellViewModel> _itemsSource;

		public GroupedObservableCollection<string, BeerCellViewModel> ItemsSource {
			get {
				return _itemsSource;
			}
			set {
				UpdateTitle ();
				SetObservableProperty (ref _itemsSource, value);
			}
		}

		private object _selectedItem;

		public object SelectedItem {
			get {
				return _selectedItem;
			}
			set {
				SetObservableProperty (ref _selectedItem, value);
				// should clear searchbox and unfocus
				if (_selectedItem == null) {
					return;
				}
				var beerCellViewModel = (BeerCellViewModel)_selectedItem;
				var beerViewModel = new BeerViewModel (_repository);
				var page = beerViewModel.ResolvePage (beerCellViewModel.BeerId);
				Navigation.PushAsync (page);
				SelectedItem = null;
			}
		}

		string _searchText = "";

		public string SearchText { 
			get {
				return _searchText;
			}
			set {
				SetObservableProperty (ref _searchText, value);
				SetBeersFiltered ();
			}
		}

		private DataTemplate _itemTemplate;

		public DataTemplate ItemTemplate {
			get { 
				return _itemTemplate;
			}
			set {
				SetObservableProperty (ref _itemTemplate, value);
			}
		}

		// what to do as the user types in searchbox
		void SetBeersFiltered ()
		{
//			using (var timer = Tracking.TrackTime ()) {
			if (BeersGrouped == null) {
				return;
			}

			var matchText = "";
			if (SearchText != null) {
				matchText = SearchText.ToLower ();
			}

			ItemsSource = BeersGrouped.Filter (
				(BeerCellViewModel beer) => 
				beer.Name.ToLower ().Contains (matchText) ||
				beer.Style.ToLower ().Contains (matchText)
			);
									
			Logger.Debug ("Updating filtered list of {0} beers", ItemsSource.Count ());
			UpdateTitle ();
//			}
		}

		public Command<object> AddBeerCommand {
			get {
				return new Command<object> (ExecuteAddBeerCommand);
			}
		}

		void ExecuteAddBeerCommand (object obj)
		{
			throw new NotImplementedException ();
		}
	}
}

