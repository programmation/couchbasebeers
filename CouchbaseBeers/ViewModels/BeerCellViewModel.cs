﻿using System;
using PropertyChanged;
using Xamarin.Forms;
using Models;

namespace ViewModels
{
	//	[ImplementPropertyChanged]
	public class BeerCellViewModel
		: BaseViewModel, IGroupableViewModel
	{
		public string BeerId { get; set; }

		public string BreweryId { get; set; }

		public string Name { get; set; }

		public double AlcoholByVolume { get; set; }

		public string Description { get; set; }

		public string Style { get; set; }

		public Beer Beer { get; private set; }

		public BeerCellViewModel ()
		{
		}

		public BeerCellViewModel (Beer beer)
		{
			Beer = beer;

			BeerId = beer.Id;
			BreweryId = beer.BreweryId;
			Name = beer.Name;
			AlcoholByVolume = beer.Abv;
			Description = beer.Description;
			Style = beer.Style;
		}

		public string NameSort {
			get {
				if (string.IsNullOrWhiteSpace (Name) || Name.Length == 0)
					return "?";

				return Name [0].ToString ().ToUpper ();
			}
		}

		public override string ToString ()
		{
			return string.Format ("[BeerCellViewModel: Name={0}, NameSort={1}]", Name, NameSort);
		}
	}
}

