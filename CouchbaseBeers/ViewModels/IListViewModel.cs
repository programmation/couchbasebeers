﻿using System;
using Xamarin.Forms;
using System.Collections;
using System.Collections.Generic;
using ObservableGrouping;

namespace ViewModels
{
	public interface IListViewModel<TCellViewModel>
		where TCellViewModel : BaseViewModel
	{
		GroupedObservableCollection<string, TCellViewModel> ItemsSource { get; }

		//		DataTemplate ItemTemplate { get; }

		object SelectedItem { get; set; }
	}

	public interface IGroupedListViewModel<TCellViewModel>
		: IListViewModel<TCellViewModel>
			where TCellViewModel : BaseViewModel
	{
		//		DataTemplate GroupHeaderTemplate { get; }
	}

	public interface IGroupableViewModel
	{
		string NameSort { get; }
	}
}

