﻿using System;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Models;

namespace Services
{
	public interface IRepository
	{
		IDataService DataService { get; }

		AppConfig GetAppConfig ();

		void SaveAppConfig (AppConfig appConfig);

		ObservableCollection<Beer> GetAllBeers ();

		Beer GetBeer (string beerId);

		void SaveBeer (Beer beer);

		Brewery GetBrewery (string breweryId);

		ObservableCollection<Beer> GetBreweryBeers (Brewery brewery);
	}
}

