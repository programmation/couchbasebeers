﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models;

namespace Services
{
	public interface ICouchbaseUserService
	{
		Task<bool> CreateUser (string authServerUrl, string username, string password);

		Task<CouchbaseUser> GetUser (string authServerUrl, string name);
	}
}

