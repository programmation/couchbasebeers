﻿using System;
using Couchbase.Lite;
using System.Threading.Tasks;
using Models;

namespace Services
{
	public interface IDataService
	{
		bool IsSyncing { get; }

		Database Database { get; }

		Task InitializeServerConnectionAsync ();

		Task InitializeServerConnectionAsync (bool autoStart);

		Task CloseServerConnectionAsync ();

		Task DebugLocalDatabaseInfoAsync ();

		AppConfig GetAppConfig ();

		void SaveAppConfig (AppConfig appConfig);

		void StartSync ();

		void StopSync ();
	}
}

