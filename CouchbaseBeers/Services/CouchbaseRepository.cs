﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Couchbase.Lite;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CouchbaseBeers;
using Extensions;
using Models;

namespace Services
{
	public class CouchbaseRepository
		: IRepository
	{
		private IDataService _dataService;

		public IDataService DataService {
			get { 
				return _dataService;
			}
		}

		public CouchbaseRepository (IDataService dataService)
		{
			_dataService = dataService;
		}

		private Task<TResult> FromResult<TResult> (TResult result)
		{
			return Task.FromResult (result);
		}

		private void SaveDocument (string docId, IDictionary<string, object> dict, string doctype)
		{
			Logger.Debug ("Saving [{0}] with Id [{1}]", doctype, docId);
			var doc = _dataService.Database.GetExistingDocument (docId);
			if (doc == null) {
				doc = _dataService.Database.GetDocument (docId);
				dict.Add ("type", doctype);
				doc.PutProperties (dict);
			} else {
				var old = doc.Properties;
				var merged = dict.Concat (old.Where (kvp => !dict.ContainsKey (kvp.Key))).ToDictionary (x => x.Key, x => x.Value);
				doc.PutProperties (merged);
			}
		}

		T GetExistingDocumentAsObject<T> (string documentId)
			where T : class, new()
		{
			T result = null;
			var doc = _dataService.Database.GetExistingDocument (documentId);
			if (doc == null) {
				return null;
			}
			result = doc.Properties.ToObject<T> ();
			return result;
		}

		T GetExistingDocumentAsObject<T> (string documentId, string propertyName) where T : class
		{
			var doc = _dataService.Database.GetExistingDocument (documentId);
			return GetDocumentProperty<T> (doc, propertyName);
		}

		T GetDocumentProperty<T> (Document doc, string propertyName) where T : class
		{
			return doc == null ? null : GetTypedObject<T> (doc.GetProperty (propertyName));
		}

		T GetTypedObject<T> (object obj)
		{
			return obj is T ? (T)obj : JsonConvert.DeserializeObject<T> (obj.ToString ());
		}

		byte [] GetBytes (object obj)
		{
			if (obj == null)
				return null;

			if (obj is byte[])
				return obj as byte[];

			if (obj is string)
				return Convert.FromBase64String (obj.ToString ());

			return null;
		}

		#region IRepository implementation

		public AppConfig GetAppConfig ()
		{
			return _dataService.GetAppConfig ();
		}

		public void SaveAppConfig (AppConfig appConfig)
		{
			_dataService.SaveAppConfig (appConfig);
		}

		public ObservableCollection<Beer> GetAllBeers ()
		{
			Func<QueryRow, Beer> transform;
			string view;

			view = CouchbaseDataService.AllBeersView;
			transform = row => {
				var k = row.Key as JArray;
				var key = k.ToObject<string[]> ();
				var id = key [0].ToString ();

				var v = row.Value as object[];
				var name = v [1].ToString ();
				var breweryId = v [2].ToString ();
				var abv = 0.0;
				double.TryParse (v [3].ToString (), out abv);
				var style = v [4] != null ? v [4].ToString () : "Unknown Style";
				var category = v [5] != null ? v [5].ToString () : "Unknown Category";

				var item = new Beer () {
					Id = id,
					Name = name,
					BreweryId = breweryId,
					Abv = abv,
					Style = style,
					Category = category,
				};

				return item;
			};
			#region Unused
//				view = CouchbaseDataService.AllBeersView2;
//				transform = row => {
//					var k = row.Key as JArray;
//					var key = k.ToObject<string[]> ();
//					var id = key [0].ToString ();
//
//					var v = row.Value as object[];
//					var name = v [0].ToString ();
//					var breweryId = v [1].ToString ();
//					var abv = 0.0;
//					double.TryParse (v [2].ToString (), out abv);
//					var style = v [3] != null ? v [3].ToString () : "Unknown Style";
//					var category = v [4] != null ? v [4].ToString () : "Unknown Category";
//
//					var item = new Beer () {
//						Id = id,
//						Name = name,
//						BreweryId = breweryId,
//						Abv = abv,
//						Style = style,
//						Category = category,
//					};
//
//					return item;
//				};
			#endregion

			var db = _dataService.Database;

			var list = db.GetExistingView (view);
			var q = list.CreateQuery ();
			var query = q.ToLiveQuery ();
			query.GroupLevel = 1;
							
			var result = new LiveQueryObservableCollection<Beer> (query, transform);
			return result;
		}

		public Beer GetBeer (string beerId)
		{
			return GetExistingDocumentAsObject<Beer> (beerId);
		}

		public void SaveBeer (Beer beer)
		{
			var key = beer.Id;
			SaveDocument (key, beer.ToDictionary (), "beer");
		}

		public Brewery GetBrewery (string breweryId)
		{
			// TODO
			return new Brewery ();
		}

		public ObservableCollection<Beer> GetBreweryBeers (Models.Brewery brewery)
		{
			throw new NotImplementedException ();
		}

		#endregion
	}
}

