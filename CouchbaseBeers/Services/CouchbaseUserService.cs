﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin;
using Instrumentation;
using Models;

namespace Services
{
	public class CouchbaseUserService 
		: ICouchbaseUserService
	{
		public async Task<bool> CreateUser (string authServerUrl, string name, string password)
		{
			using (var timer = Tracking.TrackTime ()) {
				try {
					var user = new User () {
						Name = name,
						Email = null,
						Password = password,
					};
					var userJson = JsonConvert.SerializeObject (user);
				
					using (var client = new HttpClient ()) {
						client.Timeout = TimeSpan.FromSeconds (5.0);
						var body = new StringContent (userJson);
						Logger.Debug ("New user: " + userJson);
						var response = await client.PutAsync (authServerUrl + "_user/" + name, body);
						if (response.StatusCode == System.Net.HttpStatusCode.Conflict) {
							Logger.Debug ("User already exists!");
							return true;
						}
						if (response.StatusCode != System.Net.HttpStatusCode.Created) {
							string content = await response.Content.ReadAsStringAsync ();
							Logger.Debug ("Problem creating couchbase user ({0}): {1}", response.StatusCode, content);
							return false;
						}
//					if (response.StatusCode == System.Net.HttpStatusCode.OK) {
//						Logger.Debug ("User created!");
//						return true;
//					}
					}
					return true;
				} catch (Exception ex) {
					Logger.Debug ("Exception creating couchbase user: " + ex.Message);
					return false;
				}
			}	
		}

		public async Task<CouchbaseUser> GetUser (string authServerUrl, string name)
		{
			using (var timer = Tracking.TrackTime ()) {
				using (var client = new HttpClient ()) {
					var response = await client.GetAsync (authServerUrl + "_user" + name);

					if (response.StatusCode != System.Net.HttpStatusCode.OK) {
						string content = await response.Content.ReadAsStringAsync ();
						Logger.Debug (content);
						return null;
					}

					string result = await response.Content.ReadAsStringAsync ();
					var dtoUser = JsonConvert.DeserializeObject<User> (result);

					// flatten DTOs into application model
					var user = new CouchbaseUser () {
						Name = dtoUser.Name
					};

					return user;
				}
			}
		}

		class User
		{
			[JsonProperty (PropertyName = "name")]
			public string Name {
				get;
				set;
			}

			[JsonProperty (PropertyName = "email")]
			public string Email {
				get;
				set;
			}

			[JsonProperty (PropertyName = "password")]
			public string Password {
				get;
				set;
			}

			public User ()
			{

			}

		}
	}
}

