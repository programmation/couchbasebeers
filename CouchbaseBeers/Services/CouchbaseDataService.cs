﻿#define CB_USER_AUTH_ENABLED
#define CB_SYNC_ENABLED

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Couchbase.Lite;
using Couchbase.Lite.Auth;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using CouchbaseBeers;
using Extensions;
using Instrumentation;
using Models;

namespace Services
{
	public class CouchbaseDataService
		: BaseService, IDataService
	{
		private readonly object _datalock = new object ();
		private Database _database;

		public CouchbaseDataService ()
		{
		}

		public Database Database {
			get { 
				return _database;
			}
		}

		private bool _isSyncing;

		public bool IsSyncing { 
			get {
				return _isSyncing;
			}
			private set {
				SetObservableProperty (ref _isSyncing, value);
				MessagingCenter.Send<IDataService> (this, "Sync");
			}
		}

		public void InitializeDatabase ()
		{
			CreateDatabase ();
			HandleFirstRunScenario ();
			SetupViews ();
		}

		public async Task InitializeServerConnectionAsync ()
		{
			var appConfig = GetOrCreateAppConfig ();
			await InitializeServerConnectionAsync (appConfig.AutoStartSync).ConfigureAwait (false);
		}

		public async Task InitializeServerConnectionAsync (bool start)
		{
			Tracking.Track ();

			var initialiser = new Action (async () => {
				var appConfig = GetOrCreateAppConfig ();
				if (string.IsNullOrEmpty (appConfig.ServerIPAddress)) {
					return;
				}

				await EnsureCouchbaseUserExists (appConfig).ConfigureAwait (false);

				var userId = appConfig.UserId;
				if (userId == null) {
					Logger.Debug ("UserId is empty!");
					return;
				}

				#if (CB_USER_AUTH_ENABLED)
				Logger.Debug ("UserId: [" + userId + "]");
				#endif

				SetupReplication (appConfig);

				if (start) {
					StartSync ();
				}
					
//				await DebugLocalDatabaseInfoAsync ();
			});

			await Task.Run (initialiser);
		}

		public async Task CloseServerConnectionAsync ()
		{
			await Task.Run (() => KillReplication ()).ConfigureAwait (false);
		}

		private void CreateDatabase ()
		{
			Tracking.Track ();
			_database = Manager.SharedInstance.GetExistingDatabase (AppConfig.DatafileName);

			if (_database == null) {
				var assembly = Assembly.GetCallingAssembly ();
				try {
					var resourceName = assembly.GetManifestResourceNames ()
						.Where (x => 
							x.EndsWith ((AppConfig.DatafileName + ".cblite"), 
						                   StringComparison.CurrentCultureIgnoreCase))
						.Single ();
					Logger.Debug ("resourceName: {0}", resourceName);
					var stream = assembly.GetManifestResourceStream (resourceName);
					Logger.Debug ("Stream length: {0}", stream.Length);
					Manager.SharedInstance.ReplaceDatabase (AppConfig.DatafileName, stream, null);
					_database = Manager.SharedInstance.GetExistingDatabase (AppConfig.DatafileName);
				} catch (Exception ex) {
					Logger.Debug (ex.Message);
					_database = Manager.SharedInstance.GetDatabase (AppConfig.DatafileName);
				}
			}

			#if false
			if (_database != null) {
				_database.Changed += (sender, e) => {
					var changes = e.Changes.ToList ();
					foreach (var change in changes) {
						Logger.Debug ("Changed document id: " + change.DocumentId);
					}
				};
			}
			#endif
		}

		private const string AppConfigDocumentId = "AppLocalConfig";
		private const string DefaultServerIPAddress = "pcbsubuntu12-01.cloudapp.net";
		private const int DefaultServerPort = 4985;
		private const int DefaultServerAdminPort = 4985;

		public AppConfig GetOrCreateAppConfig ()
		{
			var appConfig = GetAppConfig ();
			if (appConfig == null) {
				appConfig = new AppConfig ();
				appConfig.ServerIPAddress = DefaultServerIPAddress;
				appConfig.ServerPort = DefaultServerPort;
				appConfig.ServerAdminPort = DefaultServerAdminPort;
				appConfig.UserId = Guid.NewGuid ().ToString ("N");
				appConfig.UserPassword = appConfig.UserId;
				appConfig.AutoStartSync = true;
				SaveAppConfig (appConfig);
			}
			return appConfig;
		}

		public AppConfig GetAppConfig ()
		{
			lock (_datalock) {
				AppConfig doc = null;
				var dict = _database.GetExistingLocalDocument (AppConfigDocumentId);
				if (dict != null) {
					doc = dict.ToObject<AppConfig> ();
				}
				return doc;
			}
		}

		public void SaveAppConfig (AppConfig appConfig)
		{
			lock (_datalock) {
				var dict = appConfig.ToDictionary ();
				_database.PutLocalDocument (AppConfigDocumentId, dict);
			}
		}

		private void HandleFirstRunScenario ()
		{		
			Tracking.Track ();
			GetOrCreateAppConfig ();
		}

		public async Task EnsureCouchbaseUserExists (AppConfig appConfig)
		{
			Logger.Debug ("EnsureCouchbaseUserExists");
			#if (!CB_USER_AUTH_ENABLED)
			Logger.Debug ("Skipping creation of couchbase user.");
			return;
			#endif

			if (appConfig == null) {
				return;
			}

			if (appConfig.RemoteUserExists) {
				Logger.Debug ("Couchbase user already exists");
				return;
			}

			var service = App.Container.Resolve<ICouchbaseUserService> ();
			if (await service.CreateUser (
				    appConfig.ServerUserAdminEndpoint, 
				    appConfig.UserId, 
				    appConfig.UserPassword
			    ).ConfigureAwait (false)) {
				Logger.Debug ("Couchbase user created");
				appConfig.RemoteUserExists = true;
				SaveAppConfig (appConfig);
			}
		}

		public const string AllBeersView = "all-beers";
		public const string AllBeersView2 = "all-beers-alt";

		private void SetupViews ()
		{
			Tracking.Track ();
			var allBeersView = _database.GetExistingView (AllBeersView) ?? _database.GetView (AllBeersView);
			allBeersView.SetMapReduce (
				(doc, emit) => {
//					Logger.Debug ("Keys: {0}\nValues: {1}", string.Join (", ", doc.Keys), string.Join (", ", doc.Values));

					if (!doc.ContainsKey ("type")) {
						return;
					}

					if (doc ["type"].ToString () != "beer") {
						return;
					}

					emit (
						new object[] {
							doc ["_id"],
							doc ["name"],
							doc ["brewery_id"],
							doc ["abv"].ToString (),
							doc.ContainsKey ("style") ? doc ["style"] : "",
							doc.ContainsKey ("category") ? doc ["category"] : "",
						},
						doc ["_id"]);
				},
				(keys, values, rereduce) => {
//					Logger.Debug ("(keys, values, rereduce) =>: keys: {0}\nvalues: {1}", 
//						string.Join (", ", keys), string.Join (", ", values));

					//expected
					object itemId = null;
					object name = null;
					object breweryId = null;
					object abv = null;
					object style = null;
					object category = null;

					int i = 0;
					foreach (JArray key in keys) {
						var x = key.ToObject<object[]> ();
						itemId = x [0];
						name = x [1];
						breweryId = x [2];
						abv = x [3];
						style = x [4];
						category = x [5];
						i++;
					}

					var val = new object[] {
						itemId,
						name,
						breweryId,
						abv,
						style,
						category,
					};

//					Logger.Debug ("reduce out: " + val.ToString ());

					return val;
				},
				"7");

			var allBeersView2 = _database.GetExistingView (AllBeersView2) ?? _database.GetView (AllBeersView2);
			allBeersView2.SetMap (
				(doc, emit) => {
//					Logger.Debug ("Keys: {0}\nValues: {1}", string.Join (", ", doc.Keys), string.Join (", ", doc.Values));

					if (!doc.ContainsKey ("type")) {
						return;
					}

					if (doc ["type"].ToString () != "beer") {
						return;
					}

					emit (
						new object[] {
							doc.ContainsKey ("brewery_id") ? doc ["brewery_id"].ToString () : null,
							doc.ContainsKey ("category") ? doc ["category"].ToString () : null,
							doc.ContainsKey ("name") ? doc ["name"].ToString () : null,
							doc.ContainsKey ("abv") ? doc ["abv"].ToString () : null,
							doc.ContainsKey ("style") ? doc ["style"].ToString () : null,
						},
						doc ["_id"]);
				},
				"1");
		}

		private Replication _pull;
		private Replication _push;

		private void SetupReplication (AppConfig appConfig)
		{
			Logger.Debug ("SetupReplication");

			var server = new Uri (appConfig.ServerAddress);

			_pull = _database.CreatePullReplication (server);
			_push = _database.CreatePushReplication (server);

			_pull.Continuous = true;
			_push.Continuous = true;

			_pull.Changed += OnPullChanged;
			_push.Changed += OnPushChanged;

			#if (CB_USER_AUTH_ENABLED)
			_pull.Authenticator = AuthenticatorFactory
				.CreateBasicAuthenticator (appConfig.UserId, appConfig.UserPassword);
			_push.Authenticator = AuthenticatorFactory
				.CreateBasicAuthenticator (appConfig.UserId, appConfig.UserPassword);
			#endif
		}

		private void KillReplication ()
		{
			if (IsSyncing) {
				StopSync ();
			}

			_pull.Changed -= OnPullChanged;
			_push.Changed -= OnPushChanged;

			_pull = null;
			_push = null;
		}

		public void StartSync ()
		{
			Tracking.Track ();

			if (IsSyncing) {
				return;
			}

			if (_pull == null || _push == null) {
				return;
			}

			_pull.Start ();
			_push.Start ();

			IsSyncing = true;
		}

		public void StopSync ()
		{
			Tracking.Track ();

			if (!IsSyncing) {
				return;
			}

			if (_pull == null || _push == null) {
				return;
			}

			_pull.Stop ();
			_push.Stop ();

			IsSyncing = false;
		}

		void OnPullChanged (object sender, ReplicationChangeEventArgs e)
		{
			SendReplicationMessage ();
		}

		void OnPushChanged (object sender, ReplicationChangeEventArgs e)
		{
			SendReplicationMessage ();
		}

		private void SendReplicationMessage ()
		{
			MessagingCenter.Send<IDataService, ReplicationMessage> (this, 
				ReplicationMessage.ReplicationMessageTag, 
				(new ReplicationMessage (_pull, _push))
			);
		}

		public async Task DebugLocalDatabaseInfoAsync ()
		{
			var debugger = new Action (() => {
				Logger.Debug ("DocumentCount = {0}", _database.DocumentCount);
				var results = _database.CreateAllDocumentsQuery ().Run ();
				foreach (var item in results) {
					Logger.Debug ("item.DocumentId = {0}", item.DocumentId);
				}
			});

			await Task.Run (debugger);
		}
	}

	public class ReplicationMessage
	{
		public static string ReplicationMessageTag = "ReplicationMessage";

		public ReplicationStatus PullStatus { get; private set; }

		public ReplicationStatus PushStatus { get; private set; }

		public int PullCompleted { get; private set; }

		public int PullTotal { get; private set; }

		public int PushCompleted { get; private set; }

		public int PushTotal { get; private set; }

		public float Progress { get; private set; }

		public ReplicationMessage (Replication pull, Replication push)
		{
			PullStatus = pull.Status;
			PushStatus = push.Status;

			PullCompleted = pull.CompletedChangesCount;
			PullTotal = pull.ChangesCount;

			PushCompleted = push.CompletedChangesCount;
			PushTotal = push.ChangesCount;

			var progress = 0.0f;
			var total = 0;
			var completed = 0;

			if (PullStatus == ReplicationStatus.Active) {
				total += PullTotal;
				completed += PullCompleted;
			}
			if (PushStatus == ReplicationStatus.Active) {
				total += PushTotal;
				completed += PushCompleted;
			}

			if (total != 0) {
				progress = (float)completed / (float)total;
			}

			Progress = progress;
		}
	}
}

