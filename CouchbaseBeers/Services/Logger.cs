﻿using System;

namespace Services
{
	public static class Logger
	{
		public static void Debug (string message, params object[] args)
		{
			#if DEBUGLEVELDEBUG
			Log ("D", message, args);
			#endif
		}

		public static void Info (string message, params object[] args)
		{
			#if DEBUGLEVELDEBUG || DEBUGLEVELINFO
			Log ("I", message, args);
			#endif 
		}

		public static void Warn (string message, params object[] args)
		{
			#if DEBUGLEVELINFO || DEBUGLEVELWARN || DEBUGLEVELERROR
			Log ("W", message, args);
			#endif
		}

		public static void Error (string message, params object[] args)
		{
			#if DEBUGLEVELDEBUG || DEBUGLEVELINFO || DEBUGLEVELWARN || DEBUGLEVELERROR
			Log ("E", message, args);
			#endif
		}

		public static void Error (Exception exception)
		{
			#if DEBUGLEVELDEBUG || DEBUGLEVELINFO || DEBUGLEVELWARN || DEBUGLEVELERROR
			Log ("E", exception.Message);
			#endif
		}

		private static void Log (string prefix, string message, params object[] args)
		{
			var formattedMessage = prefix + ": " + string.Format (message, args);
			System.Diagnostics.Debug.WriteLine (formattedMessage);
		}
	}
}

