﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using CouchbaseBeers;

namespace Services
{
	public class BaseService
		: INotifyPropertyChanged
	{
		public BaseService ()
		{
		}

		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		protected virtual void OnPropertyChanged (string propertyName)
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
		}

		protected void SetObservableProperty<T> (
			ref T field, 
			T value,
			[CallerMemberName] string propertyName = "")
		{
			if (EqualityComparer<T>.Default.Equals (field, value))
				return;
			field = value;
			OnPropertyChanged (propertyName);
		}
	}
}

