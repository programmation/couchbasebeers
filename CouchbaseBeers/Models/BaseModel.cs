﻿using System;
using Newtonsoft.Json;

namespace Models
{
	public class BaseModel
	{
		[JsonProperty ("id")]
		public string Id { get; set; }

		[JsonProperty ("rev")]
		public string Rev { get; set; }

		[JsonProperty ("expiration")]
		public int Expiration { get; set; }

		[JsonProperty ("flags")]
		public int Flags { get; set; }

		[JsonProperty ("version")]
		public int Version{ get; set; }

		public BaseModel ()
		{ 
			Version = 1;
		}
	}
}

