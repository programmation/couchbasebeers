﻿using System;

namespace Models
{
	public class AppConfig
		: BaseModel
	{
		public const string DatafileName = "couchbase-beers";

		public string ServerIPAddress { get; set; }

		public int ServerPort { get; set; }

		public int ServerAdminPort { get; set; }

		public string UserId { get; set; }

		public string UserPassword { get; set; }

		public bool RemoteUserExists { get; set; }

		public bool AutoStartSync { get; set; }

		public AppConfig ()
		{
		}

		public string ServerAddress { 
			get {
				string s = String.Format ("http://{0}:{1}/{2}",
					           ServerIPAddress,
					           ServerPort,
					           DatafileName);
				return s;
			}
		}

		public string ServerUserAdminEndpoint { 
			get {
				string s = String.Format ("http://{0}:{1}/{2}/",
					           ServerIPAddress,
					           ServerAdminPort,
					           DatafileName);
				return s;
			}
		}

	}
}

