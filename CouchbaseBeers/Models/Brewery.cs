﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Models
{
	public class Geo
	{
		[JsonProperty ("accuracy")]
		public string Accuracy { get; set; }

		[JsonProperty ("lat")]
		public double Lat { get; set; }

		[JsonProperty ("lon")]
		public double Lon { get; set; }
	}

	public class Brewery
	{
		[JsonProperty ("name")]
		public string Name { get; set; }

		[JsonProperty ("city")]
		public string City { get; set; }

		[JsonProperty ("state")]
		public string State { get; set; }

		[JsonProperty ("code")]
		public string Code { get; set; }

		[JsonProperty ("country")]
		public string Country { get; set; }

		[JsonProperty ("phone")]
		public string Phone { get; set; }

		[JsonProperty ("website")]
		public string Website { get; set; }

		[JsonProperty ("type")]
		public string Type { get; set; }

		[JsonProperty ("updated")]
		public string Updated { get; set; }

		[JsonProperty ("description")]
		public string Description { get; set; }

		[JsonProperty ("address")]
		public List<string> Address { get; set; }

		[JsonProperty ("geo")]
		public Geo Geo { get; set; }
	}
}

