﻿using System;
using Newtonsoft.Json;

namespace Models
{
	public class Beer
		: BaseModel
	{
		[JsonProperty ("name")]
		public string Name { get; set; }

		[JsonProperty ("abv")]
		public double Abv { get; set; }

		[JsonProperty ("ibu")]
		public Int64 Ibu { get; set; }

		[JsonProperty ("srm")]
		public Int64 Srm { get; set; }

		[JsonProperty ("upc")]
		public Int64 Upc { get; set; }

		[JsonProperty ("type")]
		public string Type { get; set; }

		[JsonProperty ("brewery_id")]
		public string BreweryId { get; set; }

		[JsonProperty ("style")]
		public string Style { get; set; }

		[JsonProperty ("category")]
		public string Category { get; set; }

		[JsonProperty ("updated")]
		public DateTime Updated { get; set; }

		[JsonProperty ("description")]
		public string Description { get; set; }
	}
}

