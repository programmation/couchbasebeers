﻿using System;
using System.Collections.Generic;

namespace Models
{
	public class CouchbaseUser
	{
		public string Name {
			get; 
			set;
		}

		public IList<string> Channels {
			get; 
			set;
		}
	}
}

