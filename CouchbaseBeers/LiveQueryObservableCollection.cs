﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using Couchbase.Lite;
using System.Diagnostics;
using Services;

namespace CouchbaseBeers
{
	public class LiveQueryObservableCollection<T> 
		: ObservableCollection<T>, IDisposable
		where T : class
	{
		private readonly LiveQuery _query;

		public LiveQueryObservableCollection (LiveQuery query, Func<QueryRow,T> transform)
			: base ()
		{
			_query = query;

			query.Changed += (sender, e) => {
				ReloadRows (e.Rows, transform);
			};

			query.Start ();
		}

		private void ReloadRows (QueryEnumerator rows, Func<QueryRow,T> transform)
		{
			var collectionChangedAction = NotifyCollectionChangedAction.Add;
			Items.Clear ();
			foreach (var row in rows) {
				var o = transform (row);
				if (o != null)
					Items.Add (o);
			}
			Logger.Debug ("Updating LiveQuery with {0} rows...", rows.Count);
			OnCollectionChanged (new NotifyCollectionChangedEventArgs (collectionChangedAction, Items.ToList ()));
			OnPropertyChanged (new PropertyChangedEventArgs ("Count"));
			OnPropertyChanged (new PropertyChangedEventArgs ("Item[]"));
		}

		#region IDisposable implementation

		public void Dispose ()
		{
			_query.Stop ();
		}

		#endregion
	}
}

