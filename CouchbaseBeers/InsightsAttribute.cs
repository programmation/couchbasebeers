﻿//using System;
//using System.Reflection;
//using Xamarin;
//
//// Code template adapted from robgibbens@arteksoftware.com
//using System.Diagnostics;
//
//[module: Instrumentation.Insight]
//
//namespace Instrumentation
//{
//	[AttributeUsage (
//		AttributeTargets.Method
//		| AttributeTargets.Constructor
//		| AttributeTargets.Assembly
//		| AttributeTargets.Module)]
//	public class InsightAttribute
//		: Attribute
//	{
//		private string _methodName;
//
//		public void Init (object instance, MethodBase method, object[] args)
//		{
//			_methodName = method.DeclaringType.FullName + "." + method.Name;
//		}
//
//		public void OnEntry ()
//		{
//			var message = string.Format ("OnEntry: {0}", _methodName);
//			Logger.Debug (message);
//			Insights.Track (message);
//		}
//
//		public void OnExit ()
//		{
//			var message = string.Format ("OnExit: {0}", _methodName);
//			Logger.Debug (message);
//			Insights.Track (message);
//		}
//
//		public void OnException (Exception exception)
//		{
//			Logger.Debug (exception);
//			Insights.Report (exception);
//		}
//
//		public InsightAttribute ()
//		{
//		}
//	}
//
//	[AttributeUsage (AttributeTargets.Method | AttributeTargets.Constructor)]
//	public class InsightTimerAttribute
//		: Attribute
//	{
//		private string _methodName;
//		private ITrackHandle _timerHandle;
//
//		public void Init (object instance, MethodBase method, object[] args)
//		{
//			_methodName = method.DeclaringType.FullName + "." + method.Name;
//		}
//
//		public void OnEntry ()
//		{
//			_timerHandle = Insights.TrackTime (_methodName);
//			_timerHandle.Start ();
//			Logger.Debug ("Start timing {0}", _methodName);
//		}
//
//		public void OnExit ()
//		{
//			Logger.Debug ("Stop timing {0}", _methodName);
//			_timerHandle.Stop ();
//		}
//
//		public InsightTimerAttribute ()
//		{
//		}
//	}
//
//}
//
