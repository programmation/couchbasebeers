﻿using Xamarin.Forms;
using System.Collections;
using System.Linq;
using System.Reflection;
using ViewModels;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ObservableGrouping;

namespace Cells
{
	public class BeerHeaderCell
		: BaseCell
	{
		public static readonly float DefaultRowHeight = 30;

		private Label _beerHeaderLabel;

		public BeerHeaderCell ()
		{
			View = CreateView ();
			Height = DefaultRowHeight;
		}

		public View CreateView ()
		{
			_beerHeaderLabel = new Label {
				BackgroundColor = LookAndFeel.Colors.BeerAbvColor,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				FontFamily = LookAndFeel.Fonts.BeerCellAbvFont.FontFamily,
				FontAttributes = LookAndFeel.Fonts.BeerCellAbvFont.FontAttributes,
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				AnchorX = 10,
				AnchorY = 10
			};
					
			var layout = new StackLayout {
				BackgroundColor = LookAndFeel.Colors.BeerAbvColor,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness (5, 5, 0, 5),
				Spacing = 10,
				IsClippedToBounds = true,
				Children = {
					_beerHeaderLabel,
				}
			};

			return layout;
		}

		protected override void OnBindingContextChanged ()
		{
			base.OnBindingContextChanged ();
			if (BindingContext is ObservableGrouping<string, BeerCellViewModel>) {
				var viewModel = ((ObservableGrouping<string, BeerCellViewModel>)BindingContext) [0];
				if (viewModel is IGroupableViewModel) {
					var nameSort = ((IGroupableViewModel)viewModel).NameSort;
					_beerHeaderLabel.Text = nameSort;
				}
			}
		}
	}
}

