using System;
using Xamarin.Forms;
using System.ComponentModel;
using ViewModels;

//using Controls;
using LookAndFeel;
using System.Linq;

namespace Cells
{
	public class MenuCell 
		: ViewCell
	{
		readonly Label _label;

		public MenuCell ()
		{
			_label = CreateLabel ();

			var content = new ContentView {
				BackgroundColor = LookAndFeel.Colors.MenuCellColor,
				Padding = new Thickness (20, 0),
				Content = _label
			};

			content.GestureRecognizers.Add (CreateTapGestureRecognizer ());

			View = content;
		}

		IGestureRecognizer CreateTapGestureRecognizer ()
		{
			var recognizer = new TapGestureRecognizer ();
			recognizer.SetBinding (TapGestureRecognizer.CommandProperty, "ItemTapped");
			return recognizer;
		}

		static Label CreateLabel ()
		{
			var label = new Label {
				BackgroundColor = LookAndFeel.Colors.MenuCellColor,
				TextColor = LookAndFeel.Colors.MenuFontColor,
				FontFamily = LookAndFeel.Fonts.TextFont.FontFamily,
				FontAttributes = LookAndFeel.Fonts.TextFont.FontAttributes,
				XAlign = TextAlignment.Start,
				YAlign = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			label.SetBinding (Label.TextProperty, "Text");
			return label;
		}

		public static BindableProperty TextProperty =
			BindableProperty.Create<MenuCell, string> (
				b => b.Text,
				null);

		public string Text {
			get {
				return (string)GetValue (TextProperty);
			}
			set {
				SetValue (TextProperty, value);
			}
		}

		public static BindableProperty IsSelectedProperty =
			BindableProperty.Create<MenuCell, bool> (
				b => b.IsSelected,
				false,
				propertyChanged: (bindable, oldValue, newValue) => {
					((MenuCell)bindable)._label.TextColor = newValue
						? LookAndFeel.Colors.MenuFontHighlightedColor 
						: LookAndFeel.Colors.MenuFontColor;
				});

		public bool IsSelected {
			get { 
				return (bool)GetValue (IsSelectedProperty);
			}
			set {
				SetValue (IsSelectedProperty, value);
			}
		}
	}
}
