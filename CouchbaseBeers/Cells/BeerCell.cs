﻿using System;
using Xamarin.Forms;
using ValueConverters;
using System.Security.Cryptography.X509Certificates;
using ViewModels;
using Extensions;

namespace Cells
{
	public class BeerCell
		: BaseCell
	{
		public static readonly bool HasUnevenRows = true;
		public static readonly int DefaultRowHeight = 75;

		public BeerCell (BeerCellViewModel model)
		{
			BindingContext = model;
			View = CreateView ();
		}

		public BeerCell ()
		{
			View = CreateView ();
		}

		private Label _beerAbvLabel;
		private Label _beerNameLabel;
		private Label _beerStyleLabel;

		public View CreateView ()
		{
			_beerAbvLabel = new Label {
				BackgroundColor = LookAndFeel.Colors.BeerAbvColor,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.Start,
				FontFamily = LookAndFeel.Fonts.BeerCellAbvFont.FontFamily,
				FontAttributes = LookAndFeel.Fonts.BeerCellAbvFont.FontAttributes,
//				Text = "Abv",
				XAlign = TextAlignment.Center,
				YAlign = TextAlignment.Center,
				WidthRequest = 50,
				MinimumWidthRequest = 50,
				HeightRequest = 50,
				AnchorX = 10,
				AnchorY = 10
			};

			var converter = new AlcoholByVolumeValueConverter ("#.#", "N/A");

			_beerAbvLabel.SetBinding (Label.TextProperty, "AlcoholByVolume", converter: converter);

			_beerNameLabel = new Label {
				BackgroundColor = LookAndFeel.Colors.CellBackgroundColor,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				FontFamily = LookAndFeel.Fonts.BeerNameFont.FontFamily,
				FontAttributes = LookAndFeel.Fonts.BeerNameFont.FontAttributes,
//				Text = "Beer",
			};

			_beerNameLabel.SetBinding (Label.TextProperty, "Name");

			_beerStyleLabel = new Label {
				BackgroundColor = LookAndFeel.Colors.CellBackgroundColor,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				FontFamily = LookAndFeel.Fonts.BeerCellStyleFont.FontFamily,
				FontAttributes = LookAndFeel.Fonts.BeerCellStyleFont.FontAttributes,
//				Text = "Style",
			};

			_beerStyleLabel.SetBinding (Label.TextProperty, "Style");

			var nameStack = new StackLayout {
				Orientation = StackOrientation.Vertical,
				VerticalOptions = LayoutOptions.StartAndExpand,
				Spacing = 3,
				Children = {
					_beerNameLabel,
					_beerStyleLabel
				}
			};

			var layout = new StackLayout {
				BackgroundColor = LookAndFeel.Colors.CellBackgroundColor,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness (5, 5, 0, 5),
				Spacing = 10,
				IsClippedToBounds = true,
				Children = {
					_beerAbvLabel,
					nameStack,
				}
			};
				
			return layout;
		}

		//		protected override void OnBindingContextChanged ()
		//		{
		//			base.OnBindingContextChanged ();
		//		}
		//
		//		protected override void OnAppearing ()
		//		{
		//			base.OnAppearing ();
		//		}

		public override string ToString ()
		{
			return string.Format ("[BeerCell] {0}", _beerNameLabel.Text);
		}
	}
}

