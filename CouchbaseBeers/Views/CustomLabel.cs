﻿using System;
using Xamarin.Forms;
using System.Linq.Expressions;

namespace Views
{
	public class CustomLabel
		: Label
	{
		public CustomLabel ()
		{
			Setup ();
		}

		public CustomLabel (string textBinding)
			: this ()
		{
			this.SetBinding (Label.TextProperty, textBinding, BindingMode.OneWay);
		}

		protected void Setup ()
		{
			BackgroundColor = LookAndFeel.Colors.LabelBackgroundColor;
			FontFamily = LookAndFeel.Fonts.LabelFont.FontFamily;
			FontAttributes = LookAndFeel.Fonts.LabelFont.FontAttributes;
			HorizontalOptions = LayoutOptions.Start;
		}
	}

	public class CustomLabel<TSource>
		: CustomLabel
	{
		public CustomLabel (Expression<Func<TSource, object>> binding)
		{
			Setup ();
			this.SetBinding (Label.TextProperty, binding, BindingMode.OneWay);
		}

		public CustomLabel (Expression<Func<TSource, object>> binding, IValueConverter converter)
		{
			Setup ();
			this.SetBinding (Label.TextProperty, binding, BindingMode.OneWay, converter: converter);
		}
	}
}

