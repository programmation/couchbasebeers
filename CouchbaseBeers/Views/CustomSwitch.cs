﻿using System;
using Xamarin.Forms;
using System.Linq.Expressions;

namespace Views
{
	public class CustomSwitch
		: Switch
	{
		public CustomSwitch ()
		{
			Setup ();
		}

		protected void Setup ()
		{
			HorizontalOptions = LayoutOptions.End;
		}
	}

	public class CustomSwitch<TToggleSource>
		: CustomSwitch
	{
		public CustomSwitch (Expression<Func<TToggleSource, object>> toggleBinding)
		{
			Setup ();

			this.SetBinding (Switch.IsToggledProperty, toggleBinding, BindingMode.TwoWay);
		}
	}
}

