﻿using System;
using Xamarin.Forms;

namespace Views
{
	public class CustomSearchBar 
		: SearchBar
	{
		// Use Bindable properties to maintain XAML binding compatibility

		public static readonly BindableProperty BarTintProperty = BindableProperty.Create<CustomSearchBar, Color?> (p => p.BarTint, null);

		public Color? BarTint {
			get { return (Color?)GetValue (BarTintProperty); }
			set { SetValue (BarTintProperty, value); }
		}
	}
}

