﻿using System;
using Xamarin.Forms;
using Elements;
using System.Linq.Expressions;

namespace Views
{
	public class LabelledSwitch<TLabelSource, TToggleSource>
		: StackLayout
	{
		private CustomElements _elements = new CustomElements ();

		public CustomLabel Label { get; private set; }

		public CustomSwitch Switch { get; private set; }

		public LabelledSwitch (Expression<Func<TLabelSource, object>> labelBinding, 
		                       Expression<Func<TToggleSource, object>> toggleBinding)
		{
			Label = new CustomLabel<TLabelSource> (labelBinding);
			Switch = new CustomSwitch<TToggleSource> (toggleBinding);

			var container = new RelativeLayout {
				HorizontalOptions = LayoutOptions.Fill,
			};

			container.Children.Add (Label,
				Constraint.Constant (0),
				Constraint.Constant (0),
				Constraint.RelativeToParent ((parent) => {
					return (parent.Width - _elements.PadLeft - _elements.PadRight) * 1.0f / 4.0f;
				})
			);

			container.Children.Add (Switch,
				Constraint.RelativeToView (Label,
					(parent, sibling) => {
						return sibling.X + sibling.Width + 2 * Spacing;
					}),
				Constraint.Constant (0),
				Constraint.RelativeToParent ((parent) => {
					return (parent.Width - _elements.PadLeft - _elements.PadRight) * 3.0f / 4.0f;
				})
			);

			Orientation = StackOrientation.Horizontal;
			HorizontalOptions = LayoutOptions.StartAndExpand;
			Spacing = Spacing;
			Padding = new Thickness (_elements.PadLeft, _elements.PadTop, _elements.PadRight, _elements.PadBottom);
			Children.Add (container);

		}
	}

	public class LabelledSwitch<TSource>
		: LabelledSwitch<TSource, TSource>
	{
		public LabelledSwitch (Expression<Func<TSource, object>> labelBinding, 
		                       Expression<Func<TSource, object>> toggleBinding)
			: base (labelBinding, toggleBinding)
		{
		}
	}
}

