﻿using System;
using Xamarin.Forms;
using System.Linq.Expressions;

namespace Views
{
	public class CustomEditor
		: Editor
	{
		public CustomEditor ()
			: this (LookAndFeel.Fonts.EditorFont)
		{
		}

		public CustomEditor (string textBinding, float lines)
			: this (LookAndFeel.Fonts.EditorFont)
		{
			this.SetBinding (TextProperty, textBinding, BindingMode.TwoWay);
			this.Lines = lines;
		}

		public CustomEditor (Font font)
		{
			Font = font;
			BackgroundColor = LookAndFeel.Colors.EditorBackgroundColor;
			HorizontalOptions = LayoutOptions.StartAndExpand;
			VerticalOptions = LayoutOptions.StartAndExpand;
		}

		public static readonly BindableProperty FontProperty =
			BindableProperty.Create ("Font", typeof(Font), typeof(CustomEditor), new Font ());

		public static readonly BindableProperty TextColorProperty =
			BindableProperty.Create ("TextColor", typeof(Color), typeof(CustomEditor), Color.Black);

		public static readonly BindableProperty MaxWidthProperty =
			BindableProperty.Create<CustomEditor, float> (x => x.MaxWidth, (float)0);

		public static readonly BindableProperty LinesProperty =
			BindableProperty.Create<CustomEditor, float> (x => x.Lines, (float)0);

		public Font Font {
			get { return (Font)GetValue (FontProperty); }
			set { SetValue (FontProperty, value); }
		}

		public Color TextColor {
			get { return (Color)GetValue (TextColorProperty); }
			set { SetValue (TextColorProperty, value); }
		}

		public float MaxWidth {
			get { return (float)GetValue (MaxWidthProperty); }
			set { SetValue (MaxWidthProperty, value); }
		}

		public float Lines {
			get { return (float)GetValue (LinesProperty); }
			set { SetValue (LinesProperty, value); }
		}
	}

	public class CustomEditor<TSource>
		: CustomEditor
	{
		public CustomEditor (Expression<Func<TSource, object>> binding, float lines)
			: base (LookAndFeel.Fonts.EditorFont)
		{
			this.SetBinding (TextProperty, binding, BindingMode.TwoWay);
			this.Lines = lines;
		}


	}
}

