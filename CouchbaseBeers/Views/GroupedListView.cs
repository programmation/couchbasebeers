﻿using System;
using ViewModels;
using Cells;
using Xamarin.Forms;
using System.Diagnostics;
using Services;

namespace Views
{
	public class GroupedListView<TListViewModel, TCellViewModel, TCell>
		: CustomListView<TListViewModel, TCellViewModel, TCell>
		where TListViewModel : IListViewModel<TCellViewModel>
		where TCellViewModel : BaseViewModel
		where TCell : BaseCell
	{
		public GroupedListView ()
		{
			IsGroupingEnabled = true;
			GroupDisplayBinding = new Binding ("Key");
			GroupShortNameBinding = new Binding ("Key");
		}

		protected override Cell CreateDefault (object item)
		{
			//			var cell = Activator.CreateInstance<TCell> ();
			//			return cell;
			//			var itemsSource = item as GroupedObservableCollection<string, TCellViewModel>;
			//			if (itemsSource != null) {
			//				var viewModel = itemsSource [0] [0];
			//				var cell2 = ViewModelExtensions.ResolveCell (viewModel);
			//				if (cell2 != null) {
			//					return cell;
			//				}
			//			}
			var cell = base.CreateDefault (item);
			Logger.Debug (string.Format ("CreateDefault {0} -> {1}", item, cell));

			return base.CreateDefault (item);
		}
	}

	public class GroupedListView<TListViewModel, TCellViewModel, TCell, THeaderCell>
		: GroupedListView<TListViewModel, TCellViewModel, TCell>
		where TListViewModel : IListViewModel<TCellViewModel>
		where TCellViewModel : BaseViewModel
		where TCell : BaseCell
		where THeaderCell : BaseCell
	{
		public GroupedListView ()
		{
			if (Device.OS != TargetPlatform.WinPhone) {
				GroupHeaderTemplate = new DataTemplate (typeof(THeaderCell));
			}
		}
	}
}

