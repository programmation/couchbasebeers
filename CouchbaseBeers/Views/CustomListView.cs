﻿using System;
using ViewModels;
using Cells;
using Xamarin.Forms;
using System.ComponentModel;
using System.Diagnostics;
using Services;

namespace Views
{
	public class CustomListView<TListViewModel, TCellViewModel, TCell>
		: ListView
		where TListViewModel : IListViewModel<TCellViewModel>
		where TCellViewModel : BaseViewModel
		where TCell : BaseCell
	{
		public CustomListView ()
		{
			var cellType = typeof(TCell);
			ItemTemplate = new DataTemplate (cellType);

			#if DEBUG
			PropertyChanged += (sender, e) => {
				//				var list = (ListView)sender;
				var propArgs = (PropertyChangedEventArgs)e;
				var propName = propArgs.PropertyName;
				if (propName == "Renderer") {
					var test = 1;
				}
				var thisType = this.GetType ();
				var propInfo = thisType.GetProperty (propName);
				if (propInfo == null) {
					Logger.Debug (string.Format ("Property changed: {0} -> null", propName));
				} else {
					var valueType = propInfo.PropertyType;
					var value = propInfo.GetValue (this);
					Logger.Debug (string.Format ("Property changed: {0} -> {1}", propName, value));
				}
			};

			ItemAppearing += (sender, e) => {
				//				var list = (ListView)sender;
				var ev = e as ItemVisibilityEventArgs;
				var item = ev.Item;
				Logger.Debug (string.Format ("Appearing {0}", item));
			};

			ItemDisappearing += (sender, e) => {
				//				var list = (ListView)sender;
				var ev = e as ItemVisibilityEventArgs;
				var item = ev.Item;
				Logger.Debug (string.Format ("Disappearing {0}", item));
			};
			#endif

			this.SetBinding (ListView.ItemsSourceProperty, (IListViewModel<TCellViewModel> vm) => vm.ItemsSource);
			this.SetBinding (ListView.SelectedItemProperty, (IListViewModel<TCellViewModel> vm) => vm.SelectedItem, BindingMode.TwoWay);
		}

		protected override void SetupContent (Cell content, int index)
		{
			base.SetupContent (content, index);
			Logger.Debug (string.Format ("SetupContent {1},{0}", index, content));
		}
	}

	public class CustomListView<TListViewModel, TCellViewModel, TCell, THeaderCell>
		: CustomListView<TListViewModel, TCellViewModel, TCell>
		where TListViewModel : IListViewModel<TCellViewModel>
		where TCellViewModel : BaseViewModel
		where TCell : BaseCell
	{
		public CustomListView ()
		{
			var cellType = typeof(THeaderCell);
			GroupHeaderTemplate = new DataTemplate (cellType);
		}
	}

}

