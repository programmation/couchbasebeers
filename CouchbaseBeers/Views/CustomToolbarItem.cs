﻿using System;
using Xamarin.Forms;

namespace Views
{
	public class CustomToolbarItem
		: ToolbarItem
	{
		public CustomToolbarItem ()
		{
		}

		public static readonly BindableProperty CaptionProperty = 
			BindableProperty.Create<CustomToolbarItem, string> (x => x.Caption, "");

		public string Caption {
			get { 
				return (string)GetValue (CaptionProperty);
			}
			set {
				Text = value;
				SetValue (CaptionProperty, value);
			}
		}

		//		public static readonly BindableProperty IconNameProperty =
		//			BindableProperty.Create<CustomToolbarItem, string> (x => x.IconName, "");
		//
		//		public FileImageSource IconName {
		//			get { return (FileImageSource)GetValue (IconNameProperty); }
		//			set {
		//				Icon = value;
		//				SetValue (IconNameProperty, value);
		//			}
		//		}

	}
}

