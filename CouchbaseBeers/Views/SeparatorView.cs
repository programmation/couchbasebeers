﻿using System;
using Xamarin.Forms;

namespace Views
{
	public class SeparatorView
		: BoxView
	{
		public SeparatorView ()
		{
			Color = Color.FromHex ("c8c7cc");
			HeightRequest = 1;
			HorizontalOptions = LayoutOptions.FillAndExpand;
		}
	}
}

