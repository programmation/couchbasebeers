﻿using System;
using Xamarin.Forms;
using System.Linq.Expressions;

namespace Views
{
	public class CustomButton
		: Button
	{
		public CustomButton ()
		{
		}

		public CustomButton (string caption, string commandBinding, bool isEnabled = true)
		{
			Text = caption;
			this.SetBinding (Button.CommandProperty, commandBinding);
			this.IsEnabled = isEnabled;
		}
	}

	public class CustomButton<TLabelSource, TCommandSource>
		: CustomButton
	{
		public CustomButton (Expression<Func<TLabelSource, object>> captionBinding, 
		                     Expression<Func<TCommandSource, object>> commandBinding, 
		                     bool isEnabled = true)
		{
			this.SetBinding (Button.TextProperty, captionBinding);
			this.SetBinding (Button.CommandProperty, commandBinding);
			this.IsEnabled = isEnabled;
		}
	}

	public class CustomButton<TSource>
		: CustomButton<TSource, TSource>
	{
		public CustomButton (Expression<Func<TSource, object>> captionBinding, 
		                     Expression<Func<TSource, object>> commandBinding, 
		                     bool isEnabled = true)
			: base (captionBinding, commandBinding, isEnabled)
		{
		}
	}
}
