﻿using System;
using Xamarin.Forms;
using System.Linq.Expressions;
using Elements;

namespace Views
{
	public class LabelledEditor<TLabelSource, TEditorSource>
		: StackLayout
	{
		private CustomElements _elements = new CustomElements ();

		public CustomLabel Label { get; private set; }

		public CustomEditor Editor { get; private set; }

		public LabelledEditor (Expression<Func<TLabelSource, object>> labelBinding, 
		                       Expression<Func<TEditorSource, object>> editorBinding,
		                       float lines = 1.0f)
		{
			Label = new CustomLabel<TLabelSource> (labelBinding);
			Editor = new CustomEditor<TEditorSource> (editorBinding, lines);

			var container = new RelativeLayout {
				HorizontalOptions = LayoutOptions.Fill,
			};

			container.Children.Add (Label,
				Constraint.Constant (0),
				Constraint.Constant (0),
				Constraint.RelativeToParent ((parent) => {
					return (parent.Width - _elements.PadLeft - _elements.PadRight) * 1.0f / 4.0f;
				})
			);

			container.Children.Add (Editor,
				Constraint.RelativeToView (Label, 
					(parent, sibling) => {
						return sibling.X + sibling.Width + 2 * _elements.Spacing;
					}),
				Constraint.Constant (0),
				Constraint.RelativeToParent ((parent) => {
					return (parent.Width - _elements.PadLeft - _elements.PadRight) * 3.0f / 4.0f;
				})
			);

			Orientation = StackOrientation.Horizontal;
			HorizontalOptions = LayoutOptions.StartAndExpand;
			Spacing = _elements.Spacing;
			Padding = new Thickness (_elements.PadLeft, 
				_elements.PadTop, 
				_elements.PadRight, 
				_elements.PadBottom);

			Children.Add (container);
		}
	}

	public class LabelledEditor<TSource>
		: LabelledEditor<TSource, TSource>
	{
		public LabelledEditor (Expression<Func<TSource, object>> labelBinding, 
		                       Expression<Func<TSource, object>> editorBinding,
		                       float lines = 1.0f)
			: base (labelBinding, editorBinding, lines)
		{
		}
	}
}

