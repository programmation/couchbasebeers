﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;

namespace ObservableGrouping
{
	public class ObservableGrouping<TKey, TValue> : ObservableCollection<TValue>
	{
		public TKey Key { get; private set; }

		public ObservableGrouping (TKey key, IEnumerable<TValue> items)
		{
			Key = key;
			foreach (var item in items) {
				Items.Add (item);
			}
		}
	}


	public class GroupedObservableCollection<TKey, TValue>
		: ObservableCollection<ObservableGrouping<TKey, TValue>>
		where TKey : class
		where TValue : class
	{
		public GroupedObservableCollection (IEnumerable<ObservableGrouping<TKey, TValue>> groupedCollection)
			: base (groupedCollection)
		{
		}

		public GroupedObservableCollection<TKey, TValue> Filter (Func<TValue, bool> predicate)
		{
			var result = new GroupedObservableCollection<TKey, TValue> (
				             this.Select (
					             group => new ObservableGrouping<TKey, TValue> (
						             group.Key, 
						             group.Where (predicate)
					             )
				             )
				.Where (group => group.Any ())
			             );

			return result;
		}
	}
}

