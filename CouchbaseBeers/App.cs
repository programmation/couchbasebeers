﻿using System;
using TinyIoC;
using Services;
using System.Threading.Tasks;
using Pages;
using Instrumentation;
using Xamarin.Forms;
using Extensions;
using ViewModels;

namespace CouchbaseBeers
{

	/* Couchbase logo: http://docs.couchbase.com/developer/assets/images/logo.svg
	 * Beer mug: http://commons.wikimedia.org/wiki/File:Emoji_u1f37a.svg 
	 */

	public class App
		: Application
	{
		public App ()
		{
			MainPage = CreateMainPage ();
		}

		public static void Initialize ()
		{
			Tracking.Track ();
			var userService = new CouchbaseUserService ();

			var dataService = new CouchbaseDataService ();
			dataService.InitializeDatabase ();

			var repository = new CouchbaseRepository (dataService);
			var rootViewModel = new RootViewModel (repository);

			TinyIoCContainer.Current.Register<ICouchbaseUserService> (userService);
			TinyIoCContainer.Current.Register<IDataService> (dataService);
			TinyIoCContainer.Current.Register<IRepository> (repository);

			TinyIoCContainer.Current.Register<RootViewModel> (rootViewModel);
		}

		public static async Task InitializeAsync ()
		{
			Tracking.Track ();
			var dataService = TinyIoCContainer.Current.Resolve<IDataService> ();
			await dataService
				.InitializeServerConnectionAsync (true)
				.ConfigureAwait (false);
		}

		public static void Suspend ()
		{
			Tracking.Track ();
			Task.Run (async () => await SuspendAsync ());
		}

		public static async Task SuspendAsync ()
		{
			Tracking.Track ();
			var dataService = TinyIoCContainer.Current.Resolve<IDataService> ();
			await dataService
				.CloseServerConnectionAsync ()
				.ConfigureAwait (false);
		}

		public static void Resume ()
		{
			Tracking.Track ();
			Task.Run (async () => {
				await InitializeAsync ()
					.ConfigureAwait (false);
			});
		}

		public static Page CreateMainPage ()
		{	
			Tracking.Track ();
			var rootViewModel = TinyIoCContainer.Current.Resolve<RootViewModel> ();
			return rootViewModel.ResolvePage ();
		}

		public static TinyIoCContainer Container {
			get {
				return TinyIoCContainer.Current;
			}
		}
	}
}

