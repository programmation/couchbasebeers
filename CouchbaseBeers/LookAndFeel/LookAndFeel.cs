﻿using Xamarin.Forms;
using Xamarin;

namespace LookAndFeel
{
	public static class Fonts
	{
		public static Font BaseFont = Font.SystemFontOfSize (NamedSize.Medium);
		public static Font CellFont = BaseFont;
		public static Font LabelFont = Font.SystemFontOfSize (NamedSize.Medium, FontAttributes.Bold);
		public static Font TextFont = BaseFont;
		public static Font EditorFont = BaseFont;

		public static Font BeerNameFont = Font.SystemFontOfSize (NamedSize.Medium);
		public static Font BeerCellStyleFont = Font.SystemFontOfSize (NamedSize.Medium, FontAttributes.Italic);
		public static Font BeerCellAbvFont = Font.SystemFontOfSize (NamedSize.Large, FontAttributes.Bold);
		public static Font BeerPageStyleFont = Font.SystemFontOfSize (NamedSize.Small, FontAttributes.Italic);
		public static Font BeerPageAbvFont = Font.SystemFontOfSize (NamedSize.Small, FontAttributes.Bold);
	}

	public class DeviceColor
	{
		private Color _iOS;
		private Color _android;
		private Color _winPhone;
		private Color _other;

		public DeviceColor (Color iOS, Color android, Color winPhone, Color other)
		{
			_iOS = iOS;
			_android = android;
			_winPhone = winPhone;
			_other = other;
		}

		public Color Resolve ()
		{
			switch (Device.OS) {
			case TargetPlatform.iOS:
				return _iOS;
			case TargetPlatform.Android:
				return _android;
			case TargetPlatform.WinPhone:
				return _winPhone;
			case TargetPlatform.Other:
				return _other;
			}
			return Color.Default;
		}

	}

	public static class Colors
	{
		public static Color PageBackgroundColor = new DeviceColor (Color.White, Color.Black, Color.Black, Color.White).Resolve ();
		public static Color PageHeaderBackgroundColor = PageBackgroundColor;
		public static Color SearchBarTintColor = Color.FromRgb (247, 247, 247);
		public static Color CellBackgroundColor = PageBackgroundColor;
		public static Color CellFontColor = new DeviceColor (Color.Black, Color.White, Color.White, Color.Black).Resolve ();
		public static Color BeerAbvColor = new DeviceColor (Color.Gray, Color.Navy, Color.Navy, Color.Navy).Resolve ();
		public static Color LabelBackgroundColor = PageBackgroundColor;
		public static Color LabelFontColor = CellFontColor;
		public static Color EditorBackgroundColor = new DeviceColor (Color.FromHex ("CCCCCC"), Color.FromHex ("333333"), Color.FromHex ("333333"), Color.FromHex ("CCCCCC")).Resolve ();
		public static Color EditorFontColor = CellFontColor;
		public static Color MenuCellColor = new DeviceColor (Color.Black, Color.White, Color.White, Color.Black).Resolve ();
		public static Color MenuFontColor = new DeviceColor (Color.White, Color.Black, Color.Black, Color.White).Resolve ();
		public static Color MenuFontHighlightedColor = Color.Yellow;
		public static Color ReplicationStatusBackgroundColor = Color.Transparent;
		public static Color ReplicationStatusTextColor = Color.White;
		public static Color NavigationBarBackgroundColor = Color.White;
		public static Color NavigationBarTextColor = Color.FromHex ("EA2228");
	}
}
