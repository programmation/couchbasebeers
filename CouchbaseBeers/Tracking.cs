﻿using System;
using System.Runtime.CompilerServices;
using Xamarin;
using System.Collections.Generic;
using System.Diagnostics;
using Services;

namespace Instrumentation
{
	public static class Tracking
	{
		public static string ApplicationVersion = "1.1";

		public static void Track ([CallerMemberName] string memberName = "", 
		                          [CallerFilePath] string sourceFilePath = "",
		                          [CallerLineNumber] int sourceLineNumber = 0)
		{
			#if DEBUG
			Logger.Debug (memberName);
			#endif
			#if TRACKING
			Insights.Track (memberName, new Dictionary<string, string> {
				{ "Source", sourceFilePath + ":L" + sourceLineNumber.ToString () },
				{ "AppVersion", ApplicationVersion },
			});
			#endif
		}

		public static ITrackHandle TrackTime ([CallerMemberName] string memberName = "",
		                                      [CallerFilePath] string sourceFilePath = "",
		                                      [CallerLineNumber] int sourceLineNumber = 0)
		{
			#if DEBUG
			Logger.Debug (memberName);
			#endif
			#if TRACKING
			return Insights.TrackTime (memberName, new Dictionary<string, string> {
				{ "Source", sourceFilePath + ":L" + sourceLineNumber.ToString () },
				{ "AppVersion", ApplicationVersion },
			});
			#else
			return null;
			#endif
		}

		public static void TrackError (Exception exception,
		                               [CallerMemberName] string memberName = "",
		                               [CallerFilePath] string sourceFilePath = "",
		                               [CallerLineNumber] int sourceLineNumber = 0)
		{
			#if DEBUG
			Logger.Debug (memberName + ": Exception = " + exception);
			#endif
			#if TRACKING
			Insights.Report (exception, new Dictionary<string, string> {
				{ "Source", sourceFilePath + ":L" + sourceLineNumber.ToString () },
				{ "AppVersion", ApplicationVersion },
			});
			#endif
		}
	}
}

