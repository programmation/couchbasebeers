﻿using System;
using Xamarin.Forms;
using System.Linq.Expressions;
using Views;

namespace Elements
{
	public class CustomElements
	{
		public float PadLeft = 5.0f;
		public float PadTop = 5.0f;
		public float PadRight = 5.0f;
		public float PadBottom = 5.0f;
		public float Spacing = 5.0f;

		public CustomElements ()
		{
		}
	}
}

