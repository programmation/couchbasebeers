﻿using System;
using ViewModels;
using Pages;
using Xamarin.Forms;
using TinyIoC;
using Extensions;
using System.Threading.Tasks;

namespace Extensions
{
	public static class ViewModelExtensions
	{
		public static Page ResolvePage (this BaseViewModel viewModel, object data = null)
		{
			var viewModelType = viewModel.GetType ();

			var viewModelName = viewModelType.Name.Replace ("ViewModel", string.Empty);
			var pageType = Type.GetType ("Pages." + viewModelName + "Page");

			var page = (Page)TinyIoCContainer.Current.Resolve (pageType);

			page.BindingContext = viewModel;

			var pageInitialiserMethod = TypeExtensions.GetMethod (pageType, "Init");
			if (pageInitialiserMethod != null) {
				pageInitialiserMethod.Invoke (page, null);
			}

			page.Appearing += (sender, e) => {
				Task.Run (() => viewModel.Initialize (data));
			};

			return page;
		}

		public static ViewCell ResolveCell (this BaseViewModel viewModel, object data = null)
		{
			var viewModelType = viewModel.GetType ();

			var viewModelName = viewModelType.Name.Replace ("ViewModel", string.Empty);
			var cellType = Type.GetType ("Cells." + viewModelName);

			var cell = (ViewCell)TinyIoCContainer.Current.Resolve (cellType);

			cell.BindingContext = viewModel;

			var cellInitialiserMethod = TypeExtensions.GetMethod (cellType, "Init");
			if (cellInitialiserMethod != null) {
				cellInitialiserMethod.Invoke (cell, null);
			}

			cell.Appearing += (sender, e) => {
				Task.Run (() => viewModel.Initialize (data));
			};

			return cell;
		}
	}
}

