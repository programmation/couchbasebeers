﻿using System;
using System.Threading.Tasks;

namespace Extensions
{
	public static class TaskExtensions
	{
		public static void RunAsync (this Task task)
		{
			if (task.Status == TaskStatus.Created)
				task.Start ();
		}
	}
}

