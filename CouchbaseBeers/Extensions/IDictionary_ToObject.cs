﻿using System;
using System.Collections.Generic;

namespace Extensions
{
	public static class IDictionaryToObject
	{
		public static T ToObject<T> (this IDictionary<string, object> source)
			where T : class, new()
		{
			T someObject = new T ();
			Type someObjectType = someObject.GetType ();

			foreach (KeyValuePair<string, object> item in source) {
				var p = someObjectType.GetProperty (ToPascal (item.Key));
				if (p != null) {
					var pType = p.PropertyType;
					if (pType.FullName == "System.Int32") {
						Int32 value;
						Int32.TryParse (item.Value.ToString (), out value);
						p.SetValue (someObject, value, null);
					} else if (pType.FullName == "System.Double") {
						double value;
						double.TryParse (item.Value.ToString (), out value);
						p.SetValue (someObject, value, null);
					} else if (pType.FullName == "System.DateTime") {
						DateTime value;
						DateTime.TryParse (item.Value.ToString (), out value);
						p.SetValue (someObject, value, null);
					} else {
						if (p.CanWrite) {
							p.SetValue (someObject, item.Value, null);
						}
					}
				}
			}

			return someObject;
		}

		static string ToPascal (string camel)
		{
			var result = camel;

			if (string.IsNullOrEmpty (result)) {
				return result;
			}

			while (result.Substring (0, 1) == "_" && !string.IsNullOrEmpty (result)) {
				result = result.Substring (1);
			}
			var firstCharacterAsUpper = result.Substring (0, 1).ToUpper ();

			if (result.Length == 1) {
				return firstCharacterAsUpper;
			}

			return firstCharacterAsUpper + result.Substring (1);
		}
	}
}

