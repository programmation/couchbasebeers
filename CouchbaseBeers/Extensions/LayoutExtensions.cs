﻿using System;
using Xamarin.Forms;
using System.ComponentModel;

namespace Extensions
{
	public static class LayoutExtensions
	{
		/* Usage
		layout.Children.Add (view,
                    Constraint.RelativeToParent((parent) =>
                {
                    return (parent.Width/2) - (view.Width/2);   
                }),
                    Constraint.RelativeToParent((parent) =>
                {
                    return (parent.Height/2) - (view.Height/2);   
                })
		);
		UpdateConstraintsBasedOnWidth(layout, view);
		*/

		public static void UpdateConstraintsBasedOnWidth (Layout layout, View view)
		{
			view.PropertyChanged += (object sender, PropertyChangedEventArgs e) => {
				if (e.PropertyName == "Width") {
					layout.ForceLayout ();
				}
			};
		}

		public static void UpdateConstraintsBasedOnHeight (Layout layout, View view)
		{
			view.PropertyChanged += (object sender, PropertyChangedEventArgs e) => {
				if (e.PropertyName == "Height") {
					layout.ForceLayout ();
				}
			};
		}

	}
}

