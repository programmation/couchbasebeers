﻿using System;
using System.Reflection;
using System.Linq;

namespace Extensions
{
	public static class TypeExtensions
	{
		public static MethodInfo GetMethod (this Type sourceType, string methodName)
		{
			var methods = 
				sourceType.GetRuntimeMethods ()
					.Where (mi => string.Equals (methodName, mi.Name, StringComparison.Ordinal))
					.ToList ();

			return methods.FirstOrDefault ();
		}

	}
}

